package ekylibre.util.ontology;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ekylibre.database.ZeroContract.Products;
import ekylibre.database.ZeroContract.Variants;
import ekylibre.util.AccountTool;
import ekylibre.util.antlr4.QueryLanguageBaseListener;
import ekylibre.util.antlr4.QueryLanguageLexer;
import ekylibre.util.antlr4.QueryLanguageParser;

import ekylibre.zero.BuildConfig;
import ekylibre.zero.home.Zero;
import ekylibre.zero.inter.fragment.InterventionFormFragment;
import ekylibre.zero.inter.model.GenericItem;

import static ekylibre.zero.inter.InterActivity.account;

public class Grammar {

    private static final String TAG = "Grammar";
    private static final String AND = " AND ";
    private static final String OR = " OR ";

    /**
     * Entry method returning a list of desired items
     */
    public static List<GenericItem> getFilteredItems(String procedureFilter, String search, boolean isVariant) {
        if (BuildConfig.DEBUG)
            Log.i(TAG, "Looking for -> " + procedureFilter);
        final String whereClauses = buildRequest(procedureFilter, isVariant, search);
        return isVariant ? getVariants(whereClauses) : getProducts(whereClauses);
    }

    /**
     * Return a product list
     */
    private static List<GenericItem> getProducts(String whereClauses) {

        List<GenericItem> list = new ArrayList<>();

        String[] args = new String[]{AccountTool.getInstance(account)};

        // Load Inputs
        Context context = Zero.getContext();
        try (Cursor cursor = context.getContentResolver().query(Products.CONTENT_URI,
                Products.PROJECTION, whereClauses, args, Products.ORDER_BY_NAME)) {

            while (cursor != null && cursor.moveToNext()) {
                GenericItem item = new GenericItem();
                item.id = cursor.getInt(0);
                item.name = cursor.getString(1);
                item.number = cursor.getString(2);
                item.workNumber = cursor.getString(3);
                item.variety = cursor.getString(4);
                item.abilities = cursor.getString(5).split(",");
                item.population = cursor.getString(6);
                item.containerName = cursor.getString(8);
                item.netSurfaceArea = cursor.getString(9);
//                item.production_started_at = cursor.isNull(10) ? null : new Date(cursor.getLong(10));
//                item.production_stopped_at = cursor.isNull(11) ? null : new Date(cursor.getLong(11));
                item.hasHourMeter = cursor.getInt(12) == 1;
                item.born_at = cursor.isNull(10) ? null : new Date(cursor.getLong(10));
                item.dead_at = cursor.isNull(11) ? null : new Date(cursor.getLong(11));
                item.refId = cursor.isNull(13) ? null : cursor.getInt(13);
                list.add(item);
            }
        }
        return list;
    }

    /**
     * Return a variants list
     */
    private static List<GenericItem> getVariants(String whereClauses) {

        List<GenericItem> list = new ArrayList<>();
        String[] args = new String[]{AccountTool.getInstance(account)};

        // Load Inputs
        Context context = Zero.getContext();
        try (Cursor cursor = context.getContentResolver().query(Variants.CONTENT_URI,
                Variants.PROJECTION, whereClauses, args, Products.ORDER_BY_NAME)) {

            while (cursor != null && cursor.moveToNext()) {
                GenericItem item = new GenericItem();
                item.id = cursor.getInt(0);
                item.name = cursor.getString(1);
                item.number = cursor.getString(2);
                item.variety = cursor.getString(3);
                item.abilities = cursor.getString(4).split(",");
                list.add(item);
            }
        }
        return list;

    }

    /**
     *  The ANTLR procedure abilities parser
     *  which return a list of mandatory abilities
     *
     *  Huge Warning /!\ -> in prodedures *.xml assets,
     *  [plow_superficialy] in replaced with [superficialy_plow] to help ANTLR parser
     *
     *  Warning /!\ -> in animal_group_changing.xml, the [tool] filter
     *  is not handled correctly today with this method
     */
    private static List<List<String>> computeAbilitiesPhrase(String input) {

        List<List<String>> output = new ArrayList<>();

        final int[] cursor = {0};
        final int[] orLevel = {0};
        final int[] andLevel = {0};

        QueryLanguageLexer lexer = new QueryLanguageLexer(CharStreams.fromString(input));
        QueryLanguageParser parser = new QueryLanguageParser(new CommonTokenStream(lexer));
        ParseTreeWalker.DEFAULT.walk(new QueryLanguageBaseListener() {

            @Override
            public void enterTest(QueryLanguageParser.TestContext ctx) {
                super.enterTest(ctx);

                if (BuildConfig.DEBUG)
                    Log.d(TAG, "Cursor position == " + cursor[0]);

                // Check this is leaf node
                if (ctx.boolean_expression() == null) {

                    if (BuildConfig.DEBUG)
                        Log.d(TAG, "Writing test -> " + ctx.getText());

                    // Check for array at position exists
                    if (output.size() < cursor[0] + 1)
                        output.add(new ArrayList<>());

                    // If ability_parameters, get parents in tree
                    if (ctx.abilitive() != null
                            && ctx.abilitive().ability().ability_parameters() != null) {

                        String ability = ctx.abilitive().ability().ability_name().getText();
                        String abilityParameter = ctx.abilitive().ability().ability_parameters().ability_argument().get(0).getText();
                        List<String> parameterVariants = Ontology.findParentsInRealm(abilityParameter);

                        for (String param : parameterVariants)
                            output.get(cursor[0]).add(String.format("can %s(%s)", ability, param));

                    } else {
                        // Actually write the ability
                        output.get(cursor[0]).add(ctx.getText());
                    }

                    // Increment cursor in in "and" conjonction and not in "or" disjunction
                    if (orLevel[0] == 0 && andLevel[0] > 0)
                        ++cursor[0];
                }
            }

            @Override
            public void enterConjonctive(QueryLanguageParser.ConjonctiveContext ctx) {
                super.enterConjonctive(ctx);
                if (ctx.operator != null && ctx.operator.getText().equals(" and ")) {
                    ++andLevel[0];
                    if (BuildConfig.DEBUG)
                        Log.i(TAG, "enterConjonctive  \"and\" node");
                } else if (BuildConfig.DEBUG)
                    Log.i(TAG, "enterConjonctive");
            }

            @Override
            public void exitConjonctive(QueryLanguageParser.ConjonctiveContext ctx) {
                super.exitConjonctive(ctx);

                if (BuildConfig.DEBUG) {
                    if (ctx.operator != null && ctx.operator.getText().equals(" and ")) {
                        Log.i(TAG, "exitConjonctive \"and\" node");
                    } else
                        Log.i(TAG, "exitConjonctive");
                }
            }

            @Override
            public void enterDisjunctive(QueryLanguageParser.DisjunctiveContext ctx) {
                super.enterDisjunctive(ctx);
                if (ctx.operator != null && ctx.operator.getText().equals(" or ")) {
                    ++orLevel[0];
                    if (BuildConfig.DEBUG)
                        Log.d(TAG, "enterDisjunctive [OR]");
                } else if (BuildConfig.DEBUG)
                    Log.d(TAG, "enterDisjunctive");
            }

            @Override
            public void exitDisjunctive(QueryLanguageParser.DisjunctiveContext ctx) {
                super.exitDisjunctive(ctx);
                if (ctx.operator != null && ctx.operator.getText().equals(" or ")) {
                    --orLevel[0];
                    // Increment cursor when exit OR disjunctive group
                    if (orLevel[0] == 0)
                        ++cursor[0];
                    if (BuildConfig.DEBUG)
                        Log.w(TAG, "exitDisjunctive [OR]");
                } else if (BuildConfig.DEBUG)
                    Log.w(TAG, "exitDisjunctive");
            }

//            //////////
//            // Old way
//            //////////
//
//            @Override
//            public void enterEssence(QueryLanguageParser.EssenceContext ctx) {
//                super.enterEssence(ctx);
//                output.add(Collections.singletonList(ctx.getText()));
//                Log.i(TAG, "Essence -> " + ctx.getParent());
//            }
//
//            @Override
//            public void enterAbility(AbilityContext ctx) {
//                super.enterAbility(ctx);
//
//                String ability = ctx.ability_name().name().getText();
//
//                Log.v(TAG, "enterAbility ability -> " + ability);
//
//                List<String> extendedVariants = new ArrayList<>();
//
//                if (ctx.ability_parameters() != null) {
//
//                    String abilityParameter = ctx.ability_parameters().ability_argument().get(0).getText();
//                    List<String> parameterVariants = Ontology.findParentsInRealm(abilityParameter);
//
//                    for (String param : parameterVariants)
//                        extendedVariants.add("can " + ability + "(" + param + ")");
//
//                    output.add(extendedVariants);
//
//                } else {
//                    output.add(Collections.singletonList("can " + ability));
//                }
//            }


        }, parser.boolean_expression());

        for (List<String> list : output)
            if (list.contains("has indicator shape"))
                output.remove(list);

        return output;
    }

    public static List<String> computeItemAbilities(String input) {  //boolean getLowerAbilities

        List<String> output = new ArrayList<>();

        List<String> varietyParents = Ontology.findParentsInRealm(input);
        for (String variety : varietyParents)
            output.add("is " + variety);

//        QueryLanguageLexer lexer = new QueryLanguageLexer(CharStreams.fromString(input));
//        QueryLanguageParser parser = new QueryLanguageParser(new CommonTokenStream(lexer));
//        ParseTreeWalker.DEFAULT.walk(new QueryLanguageBaseListener() {
//
//            @Override
//            public void enterEssence(QueryLanguageParser.EssenceContext ctx) {
//                super.enterEssence(ctx);
//                List<String> varietyParents = Ontology.findParentsInRealm(ctx.variety_name().getText());
//                for (String variety : varietyParents)
//                    output.add("is " + variety);
//            }
//        }, parser.boolean_expression());

        return output;
    }

    /**
     *
     * @param filter The string contaning the abilities phrase (e.g. "can plow & can move")
     * @param isVariant A flag to know if it's products or variants (target differents tables)
     * @param search An optional user text search string
     * @return A valid SQL WHERE statement to request database with ContentResolver query
     */
    private static String buildRequest(String filter, boolean isVariant, String search) {

        // Prepare required abilities variants
        List<List<String>> requiredAbilities = computeAbilitiesPhrase(filter);

        // 'now' is the user setted intervention begin datetime
        final String now = String.valueOf(InterventionFormFragment.periodList.get(0).startDateTime.getTime());

        Log.i(TAG, "NOW -> "+ now);

        if (BuildConfig.DEBUG)
            Log.i(TAG, "Required abilities -> " + requiredAbilities);

        // Process search phrase
        String[] words = search != null ? StringUtils.stripAccents(search.replace("\'", "")).split("\\s") : new String[0];

        StringBuilder sb = new StringBuilder();

        // Needed to filter only current user concerned products
        sb.append("user = ?");

        // Filter on dates
        if (isVariant) {
            if (words.length > 0)
                for (String word : words)
                    sb.append(AND)
                            .append("search_name LIKE '%")
                            .append(word).append("%'");
        } else {
            sb.append(AND)
                    .append("((dead_at IS NULL OR dead_at > ")
                    .append(now).append(")")
                    .append(AND)
                    .append("born_at <= ")
                    .append(now).append(")");

            if (filter.equals("is land_parcel"))
                sb.append(AND)
                        .append("(production_stopped_on IS NULL OR production_stopped_on > ")
                        .append(now).append(")");

            if (words.length == 1)
                sb.append(AND).append("(search_name LIKE '%").append(words[0]).append("%'")
                        .append(OR).append("work_number LIKE '%").append(words[0]).append("%')");

            else if (words.length > 1)

                for (String word : words)
                    sb.append(AND).append("search_name LIKE '%").append(word).append("%'");
        }

        // Continue with filter on abilities
        if (!requiredAbilities.isEmpty()) {

            sb.append(AND);

            int sizeJ = requiredAbilities.size();

            if (sizeJ > 1)
                sb.append("(");

            for (int j = 0; j < sizeJ; j++) {

                List<String> synonymAbilities = requiredAbilities.get(j);

                int sizeI = synonymAbilities.size();
                if (sizeI > 1)
                    sb.append("(");

                for (int i = 0; i < sizeI; i++) {

                    String ability = synonymAbilities.get(i);

                    // All except first loop
                    if (i > 0 && i <= sizeI - 1)
                        sb.append(OR);
                    sb.append("abilities LIKE '%").append(ability).append("%'");
                }

                if (sizeI > 1)
                    sb.append(")");

                // All except lastest loop
                if (j < sizeJ - 1)
                    sb.append(AND);
            }

            if (sizeJ > 1)
                sb.append(")");
        }

        // Return StringBuilder as string
        return sb.toString();
    }
}
