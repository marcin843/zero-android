package ekylibre.util.pojo;

import androidx.annotation.NonNull;

import java.util.List;

import ekylibre.util.Helper;

public class SpinnerItem {

    public String name;
    public String label;
    public String unit;
    public String indicator;

    public SpinnerItem(String name, String unit, String indicator) {
        this.name = name;
        this.unit = unit;
        this.indicator = indicator;
        this.label = getHelpText();
    }

    private String getHelpText() {
        StringBuilder sb = new StringBuilder();
        if (name != null && name.equals("population"))
            sb.append(Helper.getTranslation("population_unit"));
        else {
            sb.append(Helper.getTranslation(unit));
            if (indicator != null)
                sb.append(" (")
                        .append(Helper.getTranslation(name != null ? name : indicator))
                        .append(")");
        }
        return sb.toString();
    }

    //to display object as a string in spinner
    @NonNull
    @Override
    public String toString() {
        return label;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SpinnerItem) {
            SpinnerItem item = (SpinnerItem) obj;
            return (item.unit == unit) && (item.label == label);
        }
        return false;
    }

    public static int getIndexForName(List<SpinnerItem> list, String name) {
        for (SpinnerItem item : list) {
            if (item.name.equals(name))
                return list.indexOf(item);
        }
        return 0;
    }

    public static SpinnerItem getUnitForIndicator(List<SpinnerItem> list, String indicator) {
        for (SpinnerItem item : list) {
            if (item.indicator.equals(indicator))
                return item;
        }
        return list.get(0);
    }
}
