package ekylibre.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Units {

    public static final String POPULATION = "population";
    public static final String VOLUME = "volume";
    public static final String MASS = "net_mass";
    public static final String VOLUME_AREA_DENSITY = "volume_area_density";
    public static final String MASS_AREA_DENSITY = "mass_area_density";

    public static final Unit UNITY = new Unit("unity", POPULATION, false, null, 1);

    public static final Unit LITER = new Unit("liter", VOLUME, false, null, 1);
    public static final Unit LITER_PER_HECTARE = new Unit("liter_per_hectare", VOLUME_AREA_DENSITY, true, LITER.baseUnit, 1);
    public static final Unit LITER_PER_SQUARE_METER = new Unit("liter_per_square_meter", VOLUME_AREA_DENSITY, true, LITER.baseUnit, 10000);

    public static final Unit KILOGRAM = new Unit("kilogram", MASS, false, null, 1);
    public static final Unit KILOGRAM_PER_HECTARE = new Unit("kilogram_per_hectare", MASS_AREA_DENSITY, false, KILOGRAM.baseUnit, 1);
    public static final Unit KILOGRAM_PER_SQUARE_METER = new Unit("kilogram_per_square_meter", MASS_AREA_DENSITY, false, KILOGRAM.baseUnit, 10000);

    public static final List<Unit> ALL_UNITS = new ArrayList<>(Arrays.asList(UNITY, LITER, LITER_PER_HECTARE, KILOGRAM, KILOGRAM_PER_HECTARE));
    public static final List<Unit> VOLUME_UNITS = new ArrayList<>(Arrays.asList(UNITY, LITER, LITER_PER_HECTARE));
    public static final List<Unit> MASS_UNITS = new ArrayList<>(Arrays.asList(UNITY, KILOGRAM, KILOGRAM_PER_HECTARE));

    // 'kilogram' is base unit of <net_mass>
    private static final String GRAM = "gram";
    private static final String QUINTAL = "quintal";
    private static final String TON = "ton";

    // 'liter' is base unit of <net_volume>
    private static final String MILLILITER = "milliliter";
    private static final String CENTILITER = "centiliter";
    private static final String HECTOLITER = "hectoliter";
    private static final String CUBIC_METER = "cubic_meter";

    // 'unity_per_square_meter' is base unit of <grains_area_density>
    private static final String THOUSAND_PER_HECTARE = "thousand_per_hectare";

    // '?' is base unit of <volume_density>
    private static final String CENTILITER_PER_HECTOLITER = "centiliter_per_hectoliter";

    // '?' is base unit of <mass_volume_density>
    private static final String GRAM_PER_HECTOLITER = "gram_per_hectoliter";

    // 'kilogram_per_hectare' is base unit of <mass_area_density>
    private static final String TON_PER_HECTARE = "ton_per_hectare";
    private static final String QUINTAL_PER_HECTARE = "quintal_per_hectare";

    // Base numbers
    private static final BigDecimal THOUSAND = new BigDecimal(1000);
    private static final BigDecimal HUNDRED = new BigDecimal(100);

    public static List<Unit> getUnitListForDimension(String dimension, boolean noCrop) {

        String dim = dimension == null ? "no_dim" : dimension;
        List<Unit> list = new ArrayList<>();

        if (dim.equals(VOLUME_AREA_DENSITY) || dim.equals(VOLUME)) {
            list.addAll(VOLUME_UNITS);
            if (noCrop)
                list.remove(LITER_PER_HECTARE);
            return list;

        } else if (dim.equals(MASS_AREA_DENSITY) || dim.equals(MASS)) {
            list.addAll(MASS_UNITS);
            if (noCrop)
                list.remove(KILOGRAM_PER_HECTARE);
            return list;

        } else {
            list.addAll(ALL_UNITS);
            if (noCrop) {
                list.remove(LITER_PER_HECTARE);
                list.remove(KILOGRAM_PER_HECTARE);
            }
            return list;
        }
    }
}
