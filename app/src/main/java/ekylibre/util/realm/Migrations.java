package ekylibre.util.realm;


import android.content.Context;
import android.util.Log;

import java.util.Date;

import ekylibre.util.realm.models.Procedure;
import ekylibre.util.realm.models.ProcedureAttribute;
import ekylibre.util.realm.models.ProcedureHandler;
import ekylibre.util.realm.models.Product;
import ekylibre.util.realm.models.Variant;
import ekylibre.zero.home.Zero;
import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;


public class Migrations implements RealmMigration {

    private static final String TAG = "RealmMigrations";

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        // DynamicRealm exposes an editable schema
        RealmSchema schema = realm.getSchema();

        Log.i(TAG, "Previous Realm version is -> " + oldVersion);

        if (oldVersion == 0) {
            Log.e(TAG, "Run Realm 0 -> 1 migration");

            schema.create(ProcedureHandler.tableName)
                    .addField("name", String.class, FieldAttribute.PRIMARY_KEY)
                    .addField("indicator", String.class)
                    .addField("unit", String.class);

            RealmObjectSchema procedureHandlerSchema = schema.get(ProcedureHandler.tableName);
            if (procedureHandlerSchema != null)
                schema.create(ProcedureAttribute.tableName)
                        .addField("name", String.class, FieldAttribute.PRIMARY_KEY)
                        .addField("filter", String.class)
                        .addField("cardinality", String.class)
                        .addField("group", String.class)
                        .addRealmListField("handlers", procedureHandlerSchema);
            else
                Log.e(TAG, "Error while ProcedureAttribute migration");

            RealmObjectSchema procedureAttributeSchema = schema.get(ProcedureAttribute.tableName);
            if (procedureAttributeSchema != null)
                schema.create(Procedure.tableName)
                        .addField("name", String.class, FieldAttribute.PRIMARY_KEY)
                        .addRealmListField("categories", String.class)
                        .addRealmListField("optionalActions", String.class)
                        .addRealmListField("targets", procedureAttributeSchema)
                        .addRealmListField("inputs", procedureAttributeSchema)
                        .addRealmListField("outputs", procedureAttributeSchema)
                        .addRealmListField("doers", procedureAttributeSchema)
                        .addRealmListField("tools", procedureAttributeSchema)
                        .addField("group", String.class)
                        .addField("deprecated", boolean.class);
            else
                Log.e(TAG, "Error while Procedure migration");

            schema.create(Product.tableName)
                    .addField("id", int.class, FieldAttribute.PRIMARY_KEY)
                    .addField("name", String.class)
                    .addField("number", String.class)
                    .addField("workNumber", String.class)
                    .addField("variety", String.class)
                    .addRealmListField("abilities", String.class)
                    .addField("population", String.class)
                    .addField("unit", String.class)
                    .addField("netSurfaceArea", String.class)
                    .addField("containerName", String.class)
                    .addField("deadAt", Date.class)
                    .addField("productionStartedOn", Date.class)
                    .addField("productionStoppedOn", Date.class)
                    .addField("user", String.class);

            schema.create(Variant.tableName)
                    .addField("id", int.class, FieldAttribute.PRIMARY_KEY)
                    .addField("name", String.class)
                    .addField("number", String.class)
                    .addField("variety", String.class)
                    .addRealmListField("abilities", String.class)
                    .addField("user", String.class);

            // Fill new tables with procedures schema
            Context context = Zero.getContext();



            oldVersion++;
        }

//        if (oldVersion == 1) {
//            schema.get("Person")
//                    .addField("id", long.class, FieldAttribute.PRIMARY_KEY)
//                    .addRealmObjectField("favoriteDog", schema.get("Dog"))
//                    .addRealmListField("dogs", schema.get("Dog"));
//            oldVersion++;
//        }
    }
}