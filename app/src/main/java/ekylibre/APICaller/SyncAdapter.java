package ekylibre.APICaller;

//import android.accounts.Account;
//import android.accounts.AccountManager;
//import android.accounts.AccountsException;
//import android.annotation.SuppressLint;
//import android.content.AbstractThreadedSyncAdapter;
//import android.content.ContentProviderClient;
//import android.content.ContentProviderOperation;
//import android.content.ContentResolver;
//import android.content.ContentValues;
//import android.content.Context;
//import android.content.Intent;
//import android.content.OperationApplicationException;
//import android.content.SharedPreferences;
//import android.content.SyncResult;
//import android.database.Cursor;
//import android.database.DatabaseUtils;
//import android.database.sqlite.SQLiteDatabase;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.RemoteException;
//import android.provider.DocumentsContract;
//import android.provider.MediaStore;
//import android.provider.Settings.Secure;
//import android.util.Base64;
//import android.util.Log;
//
//import androidx.preference.PreferenceManager;
//
//import com.google.android.gms.common.GoogleApiAvailability;
//import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
//import com.google.android.gms.common.GooglePlayServicesRepairableException;
//import com.google.android.gms.security.ProviderInstaller;
//
//import org.apache.commons.lang3.StringUtils;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//import java.nio.charset.StandardCharsets;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Locale;
//import java.util.Objects;
//import java.util.Set;
//import java.util.Timer;
//import java.util.TimerTask;
//import java.util.UUID;
//
//import ekylibre.database.DatabaseHelper;
//import ekylibre.exceptions.HTTPException;
//import ekylibre.util.AccountTool;
//import ekylibre.util.Contact;
//import ekylibre.util.DateConstant;
//import ekylibre.util.ImageConverter;
//import ekylibre.util.NetworkHelpers;
//import ekylibre.util.UpdatableActivity;
//import ekylibre.zero.BuildConfig;
//import ekylibre.zero.SettingsActivity;
//import ekylibre.zero.intervention.InterventionActivity;
//
//import static ekylibre.util.Helper.getTranslation;
//import static ekylibre.util.Helper.iso8601;
//import static ekylibre.util.Helper.simpleISO8601;
//import static ekylibre.zero.inter.InterActivity.FINISHED;
//import static ekylibre.database.ZeroContract.*;
//
//
///**
// * Handle the transfer of data between a server and an
// * app, using the Android sync adapter framework.
// */
//public class SyncAdapter extends AbstractThreadedSyncAdapter {
//
//    public static final String TAG = "SyncAdapter";
//    public static final String ACCOUNT_TYPE = getTranslation("accountType");  // "ekylibre.account.basic.dev";
//    public static final String SYNC_STARTED = "sync_started";
//    public static final String SYNC_FINISHED = "sync_finished";
//    public static boolean onlyVariants = false;
//    public static boolean onlyProducts = false;
//
//    private ContentResolver cr;
//    private AccountManager am;
//    private Context context;
//    private String lastSyncattribute;
//    private boolean error;
//    private Instance instance;
//    private Intent broadcastIntent;
//    private SharedPreferences prefs;
//
//    /**
//     * SyncAdapter main constructor
//     */
//    public SyncAdapter(Context context, boolean autoInitialize) {
//        super(context, autoInitialize);
//
//        this.cr = context.getContentResolver();
//        this.am = AccountManager.get(context);
//        this.context = context;
//    }
//
//    /**
//     * Set up the sync adapter. This form of the
//     * constructor maintains compatibility with Android 3.0
//     * and later platform versions
//     */
//    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
//        super(context, autoInitialize, allowParallelSyncs);
//
//        this.cr = context.getContentResolver();
//        this.am = AccountManager.get(context);
//        this.context = context;
//    }
//
//    @Override
//    public void onSyncCanceled() {
////        super.onSyncCanceled();
//        Log.e(TAG, "onSyncCanceled() -> Do nothing");
//        prefs.edit().putBoolean("syncCanceled", true).apply();
//    }
//
//    @Override
//    public void onSyncCanceled(Thread thread) {
//        super.onSyncCanceled(thread);
//        Log.e(TAG, "onSyncCanceled(thread) -> Do nothing");
//    }
//
//    @SuppressLint("ApplySharedPref")
//    @Override
//    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
//
//        // Updates certificates
//        NetworkHelpers.fixHandshakeSSLError(context);
//
//        String itemsToSync = extras.getString("itemsToSync", "");
//
//        // Send activity notification that we begin sync
//        context.sendBroadcast(new Intent(UpdatableActivity.ACTION_STARTED_SYNC));
//
//        // Get the current Account Instance
//        instance = getInstance(account);
//
//        // Reset the error flag
//        error = false;
//
//        // Get default worker_id
//        prefs = context.getSharedPreferences("prefs", Context.MODE_MULTI_PROCESS);
//
//        // Return il already syncing
//        if (prefs.getBoolean("syncCanceled", false))
//            return;
//
//        prefs.edit().putBoolean("isSyncing", true).apply();
//        getWorkerId(account, prefs);
//
//        // Run background ping
//        pingServerTask(account);
//
//        // Get last sync date from Shared prefs
//        long lastSyncDate = prefs.getLong("last_sync_date_" + account.name, 0);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.FRANCE);
//
//        if (lastSyncDate > 0) {
//            try {
//                lastSyncattribute = "?modified_since=" + URLEncoder.encode(sdf.format(new Date(lastSyncDate)), "UTF-8");
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//        }
//        else
//            lastSyncattribute = null;
//
//        // Save last try time
//        prefs.edit().putLong("last_sync_try_" + account.name, new Date().getTime()).apply();
//
//        Log.e(TAG, "LastSyncAttribute -> " + lastSyncattribute);
//
//        // Create progress status intent
////        broadcastIntent = new Intent(UpdatableActivity.ACTION_PROGRESS_SYNC);
//
////        if (onlyProducts) {
////
////            if (BuildConfig.DEBUG) Log.i(TAG, "=== Only sync products ===");
////            pullProducts(account);
////
////        } else if (onlyVariants) {
////
////            if (BuildConfig.DEBUG) Log.i(TAG, "=== Only sync variants ===");
////            pullVariants(account);
////
////        } else {
//
////        cr.delete(PlantDensityAbacusItems.CONTENT_URI, null, null);
////        cr.delete(PlantDensityAbaci.CONTENT_URI, null, null);
//
//        // Do the sequencial sync
//
//        pullPlantDensityAbaci(account);
//
//        pushIssues(account);
//
//        pushPlantCounting(account);
//
//        pushIntervention(account);
//        cr.delete(InterventionParameters.CONTENT_URI, null, null);
//
//        if (prefs.getInt(AccountTool.getEmail(account), -1) != -1)
//            pullIntervention(account);
//
//        pushObservation(account);
//
//        if (!itemsToSync.equals("variants"))
//            pullProducts(account);
////
//        if (!itemsToSync.equals("products"))
//            pullVariants(account);
//
////            cr.delete(Plants.CONTENT_URI, null, null);
//        pullPlants(account);
//
//        pushDetailedIntervention(account);
//
////            pullContacts(account);
//        cleanLocalDb(account);
//
////        }
//
//        prefs.edit()
//                .putBoolean("account_changed", false)
//                .putLong("last_sync_date_" + account.name, new Date().getTime())
//                .putLong("last_sync_try_" + account.name, new Date().getTime())
//                .commit();
//
//        prefs.edit()
//                .remove("isSyncing")
//                .remove("last_sync_try_" + account.name)
//                .remove("syncCanceled").commit();
//
////        // Save last sync date
////        if (error) {
////            prefs.edit().putLong("last_sync_date_" + account.name, 0).apply();
////            if (BuildConfig.DEBUG)
////                Log.e(TAG, "Sync error, will try again next time");
////        }
////        else {
////            prefs.edit().putBoolean("account_changed", false)
////                    .putLong("last_sync_date_" + account.name, new Date().getTime()).apply();
////            if (BuildConfig.DEBUG)
////                Log.i(TAG, "Everything ok, no sync error.");
////        }
//        onlyVariants = false;
//        onlyProducts = false;
//        context.sendBroadcast(new Intent(UpdatableActivity.ACTION_FINISHED_SYNC));
//    }
//
//    private void sendProgressNotification(String str) {
//        broadcastIntent = new Intent(UpdatableActivity.ACTION_PROGRESS_SYNC);
//        broadcastIntent.putExtra("message", str);
//        context.sendBroadcast(broadcastIntent);
//        prefs.edit().putString("current_progress_message", str).apply();
//    }
//
//    private void sendProgressNotification(int progress, int max) {
//        broadcastIntent.putExtra("progress", progress);
//        broadcastIntent.putExtra("max", max);
//        context.sendBroadcast(broadcastIntent);
//    }
//
//    private void cleanLocalDb(Account account) {
//        cr.delete(Interventions.CONTENT_URI, Interventions.USER + " LIKE " + " \"" + account.name + "\"" + " AND " +
//                Interventions.STATE + " LIKE " + "\"" + InterventionActivity.STATUS_FINISHED + "\"", null);
//    }
//
//    private void pingServerTask(Account account) {
//
//        Instance api = getInstance(account);
//
//        Timer pingTimer = new Timer();
//        pingTimer.scheduleAtFixedRate(new TimerTask() {
//            @Override
//            public void run() {
//                if (!prefs.getBoolean("isSyncing", false))
//                    pingTimer.cancel();
//                else {
//                    try {
//                        JSONObject response = api.getJSONObject("/api/v1/profile", "");
//                        Log.v(TAG, "### Ping response ### -> " + response.getInt("worker_id"));
//                    } catch (JSONException | IOException | HTTPException e) {
//                        Log.e(TAG, "### Ping error ### " + e.getMessage());
//                    }
//                }
//            }
//        },1000,3000);
//
//
//    }
//
//    private void getWorkerId(Account account, SharedPreferences prefs) {
//
//        Instance inst = getInstance(account);
//        try {
//            JSONObject json = inst.getJSONObject("/api/v1/profile", "");
//            if (json.has("worker_id")) {
//                prefs.edit().putInt(AccountTool.getEmail(account), json.getInt("worker_id")).apply();
//            } else {
//                prefs.edit().remove(AccountTool.getEmail(account)).apply();
//            }
//        } catch (JSONException | IOException | HTTPException e) {
//            Log.e(TAG, "No Worker ID, remove existing key");
//            prefs.edit().remove(AccountTool.getEmail(account)).apply();
//            e.printStackTrace();
//        }
//    }
//
//    /**
//    ** Following methods are used to transfer data between zero and ekylibre instance
//    ** There are POST and GET call
//    **/
//    private void pushIssues(Account account)
//    {
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[issues] sync started...");
//
//        sendProgressNotification("Synchronisation des incidents");
//
//        // Get crumbs from Issue (content) provider
//        try (Cursor cursor = cr.query(
//                Issues.CONTENT_URI,
//                Issues.PROJECTION_ALL,
//                Issues.USER + " LIKE " + "\"" + account.name + "\""
//                        + " AND " + IssuesColumns.SYNCED + " == " + 0,
//                null,
//                Issues.SORT_ORDER_DEFAULT)) {
//
//
//            if (cursor != null && cursor.getCount() > 0) {
//                cursor.moveToFirst();
//                while (!cursor.isAfterLast()) {
//                    try {
//                        postNewIssue(cursor, instance);
//                    } catch (JSONException | IOException | HTTPException e) {
//                        e.printStackTrace();
//                    }
//                    cursor.moveToNext();
//                }
//            } else if (BuildConfig.DEBUG)
//                Log.v(TAG, "[issues] nothing to sync.");
//        }
//
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[issues] sync done.");
//    }
//
//    private void postNewIssue(Cursor cursor, Instance instance)
//            throws JSONException, IOException, HTTPException {
//
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[issues] sync new one...");
//
//        // Post it to ekylibre
//        JSONObject attributes = new JSONObject();
//        attributes.put("nature", cursor.getString(1));
//        attributes.put("gravity", cursor.getInt(2));
//        attributes.put("priority", cursor.getInt(3));
//        attributes.put("description", cursor.getString(5));
//        attributes.put("observed_at", iso8601.format(new Date(cursor.getLong(8))));
//        //TODO : send images to ekylibre api
//        //attributes.put("images", createImageJSONArray(cursor));
//        if (cursor.getDouble(9) != 0 && cursor.getDouble(10) != 0)
//        {
//            attributes.put("geolocation", "SRID=4326; POINT(" + Double.toString(cursor.getDouble(10)) + " " + Double.toString(cursor.getDouble(9)) + ")");
//        }
//
//        long id = Issue.create(instance, attributes);
//        String db_id = Long.toString(cursor.getLong(0));
//        // Marks them as synced
//        ContentValues values = new ContentValues();
//        values.put(Issues.SYNCED, 1);
//        cr.update(Uri.withAppendedPath(Issues.CONTENT_URI, db_id), values, null, null);
//
//        values.clear();
//        values.put(ObservationIssues.EKY_ID_ISSUE, id);
//        cr.update(ObservationIssues.CONTENT_URI,
//                values, ObservationIssues.FK_ISSUE + "=?", new String[]{db_id});
//    }
//
////    private JSONArray createImageJSONArray(Cursor cursor) throws JSONException
////    {
////        JSONArray imageJSON = new JSONArray();
////        int id = cursor.getInt(0);
////        ArrayList<String> imageBlock = ImageConverter.getImagesFromIssue(id);
////        int i = -1;
////
////        while (++i < imageBlock.size())
////        {
////            JSONObject jsonObject = new JSONObject();
////            String image = imageBlock.get(i);
////            jsonObject.put("value", image);
////            imageJSON.put(jsonObject);
////        }
////        return (imageJSON);
////    }
//
//    private void pullPlantDensityAbaci(Account account) {
//
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[plant_density_abaci] sync started...");
//
//        sendProgressNotification("Synchronisation des abaques");
//
//        ContentValues cv = new ContentValues();
//
//        List<PlantDensityAbacus> abacusList = null;
//        try {
//            abacusList = PlantDensityAbacus.all(instance, null);
//        } catch (JSONException | IOException | HTTPException e) {
//            e.printStackTrace();
//        }
//        if (abacusList == null)
//            return;
//
//        Iterator<PlantDensityAbacus> abacusIterator = abacusList.iterator();
//
//        if (BuildConfig.DEBUG)
//            Log.v(TAG, "[plant_density_abaci] abacus count : " + abacusList.size() );
//
//            while(abacusIterator.hasNext()) {
//                PlantDensityAbacus plantDensityAbacus = abacusIterator.next();
//                cv.put(PlantDensityAbaciColumns.EK_ID, plantDensityAbacus.getId());
//                cv.put(PlantDensityAbaciColumns.NAME, plantDensityAbacus.getName());
//                cv.put(PlantDensityAbaciColumns.VARIETY, plantDensityAbacus.getVariety());
//                cv.put(PlantDensityAbaciColumns.ACTIVITY_ID, plantDensityAbacus.getActivityID());
//                cv.put(PlantDensityAbaciColumns.GERMINATION_PERCENTAGE, plantDensityAbacus.getGerminationPercentage());
//                cv.put(PlantDensityAbaciColumns.SEEDING_DENSITY_UNIT, plantDensityAbacus.getSeedingDensityUnit());
//                cv.put(PlantDensityAbaciColumns.SAMPLING_LENGTH_UNIT, plantDensityAbacus.getSamplingLenghtUnit());
//                cv.put(PlantDensityAbaciColumns.USER, account.name);
//                cr.insert(PlantDensityAbaci.CONTENT_URI, cv);
//            }
//        try {
//            pullPlantDensityAbacusItem(account, abacusList);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[plant_density_abaci] sync done.");
//    }
//
//    private void pullPlantDensityAbacusItem(Account account, List<PlantDensityAbacus> abacusList) throws JSONException {
//
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[plant_density_abacus_items] sync started...");
//        ContentValues cv = new ContentValues();
//
//        for (PlantDensityAbacus plantDensityAbacus : abacusList) {
//            int i = 0;
//            while (i < plantDensityAbacus.mItems.length()) {
//                cv.put(PlantDensityAbacusItemsColumns.EK_ID, plantDensityAbacus.getItemID(i));
//                cv.put(PlantDensityAbacusItemsColumns.FK_ID, plantDensityAbacus.getId());
//                cv.put(PlantDensityAbacusItemsColumns.PLANTS_COUNT, plantDensityAbacus.getItemPlantCount(i));
//                cv.put(PlantDensityAbacusItemsColumns.SEEDING_DENSITY_VALUE, plantDensityAbacus.getItemDensityValue(i));
//                cv.put(PlantDensityAbacusItemsColumns.USER, account.name);
//                cr.insert(PlantDensityAbacusItems.CONTENT_URI, cv);
//                i++;
//            }
//        }
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[plant_density_abacus_items] sync done.");
//    }
//
//
//
//    private void pullPlants(Account account) {
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[plants] sync started...");
//
//        sendProgressNotification("Synchronisation des activités");
//
//        ContentValues cv = new ContentValues();
//
//        List<Plant> plantsList = null;
//        try {
//            String lastSync = lastSyncattribute;
//            SQLiteDatabase db = new DatabaseHelper(context).getReadableDatabase();
//            final long count = DatabaseUtils.queryNumEntries(db, "plants");
//            if (count == 0)
//                lastSync = null;
//            plantsList = Plant.all(instance, lastSync);
//        } catch (JSONException | IOException | HTTPException | ParseException e) {
//            e.printStackTrace();
//        }
//
//        if (plantsList == null)
//            return;
//
//        if (BuildConfig.DEBUG)
//            Log.v(TAG, "Plants count : " + plantsList.size() );
//
//        for (Plant plants : plantsList) {
//            cv.put(Plants.EK_ID, plants.getId());
//            cv.put(Plants.NAME, plants.getName());
//            cv.put(Plants.VARIETY, plants.getVariety());
//            cv.put(Plants.ACTIVITY_ID, plants.getActivityID());
//            cv.put(Plants.ACTIVITY_NAME, plants.getmActivityName());
////            cv.put(Plants.NET_SURFACE_AREA, plants.net_surface_area);
////            cv.put(Plants.DEAD_AT, plants.deadAt != null ? plants.deadAt.getTime() : null);
//            cv.put(Plants.ACTIVE, true);
//            cv.put(Plants.USER, account.name);
//            cr.insert(Plants.CONTENT_URI, cv);
//        }
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[plants] sync done.");
//    }
//
//    private void pushPlantCounting(Account account)
//    {
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[plant_counting] sync started...");
//
//        sendProgressNotification("Synchronisation des comptages");
//
//        try (Cursor cursor = cr.query(PlantCountings.CONTENT_URI,
//                PlantCountings.PROJECTION_ALL,
//                PlantCountingsColumns.USER + " LIKE " + "\"" + account.name + "\""
//                        + " AND " + PlantCountingsColumns.SYNCED + " == " + 0,
//                null,
//                PlantCountings.SORT_ORDER_DEFAULT)) {
//
//            if (cursor != null && cursor.getCount() > 0) {
//                while (cursor.moveToNext()) {
//                    try {
//                        if (BuildConfig.DEBUG)
//                            Log.v(TAG, "New plantCounting");
//                        // Post it to ekylibre
//                        JSONObject attributes = new JSONObject();
//                        //attributes.put("geolocation", "SRID=4326; POINT(" + Double.toString(cursor.getDouble(3)) + " " + Double.toString(cursor.getDouble(2)) + ")");
//                        attributes.put("read_at", iso8601.format(new Date(cursor.getLong(1))));
//                        attributes.put("comment", cursor.getString(4));
//                        attributes.put("plant_density_abacus_item_id", cursor.getInt(5));
//                        //attributes.put("plant_density_abacus_id", cursor.getString(7));
//                        attributes.put("plant_id", cursor.getInt(8));
//                        attributes.put("average_value", cursor.getFloat(9));
//                        attributes.put("nature", cursor.getString(10));
//                        attributes.put("items_attributes", createPlantCountingItemJSON(account, cursor.getInt(0)));
//                        //attributes.put("device_uid", "android:" + Secure.getString(cr, Secure.ANDROID_ID));
//
//                        long id = PlantCounting.create(instance, attributes);
//                        ContentValues values = new ContentValues();
//                        values.put(IssuesColumns.SYNCED, 1);
//                        cr.update(Uri.withAppendedPath(PlantCountings.CONTENT_URI, Long.toString(cursor.getLong(0))), values, null, null);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//                cursor.close();
//            } else if (BuildConfig.DEBUG) Log.v(TAG, "[plant_counting] nothing to sync");
//        }
//        if (BuildConfig.DEBUG) Log.d(TAG, "[plant_counting] sync done.");
//    }
//
//    private JSONArray createPlantCountingItemJSON(Account account, int ID) {
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[plant_counting_item] sync started...");
//
//        try (Cursor cursor = cr.query(PlantCountingItems.CONTENT_URI,
//                PlantCountingItems.PROJECTION_ALL,
//                PlantCountingItemsColumns.USER + " LIKE " + "\"" + account.name + "\""
//                + " AND " + ID + " == " + PlantCountingItems.PLANT_COUNTING_ID,null,
//                PlantCountingItems.SORT_ORDER_DEFAULT)) {
//            try {
//                if (cursor != null && cursor.getCount() > 0) {
//                    JSONArray arrayJSON = new JSONArray();
//                    int i = 0;
//                    while (cursor.moveToNext()) {
//                        if (BuildConfig.DEBUG)
//                            Log.v(TAG, "[plant_counting_item] new one.");
//                        // Post it to ekylibre
//                        JSONObject attributes = new JSONObject();
//                        attributes.put("value", cursor.getInt(1));
//                    /*attributes.put("plant_id", cursor.getInt(2));
//                    attributes.put("device_uid", "android:" + Secure.getString(cr, Secure.ANDROID_ID));*/
//                        arrayJSON.put(attributes);
//                    }
//                    return (arrayJSON);
//                } else {
//                    if (BuildConfig.DEBUG)
//                        Log.v(TAG, "[plant_counting_item] nothing to sync.");
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[plant_counting_item] sync done.");
//        return (null);
//    }
//
//    private void pullIntervention(Account account)
//    {
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[interventions] GET sync started...");
//
//        List<Intervention> interventionList = null;
//        try {
//            interventionList = Intervention.all(instance, "?nature=request&user_email=" +
//                    AccountTool.getEmail(account) + "&without_interventions=true");
//        } catch (JSONException | IOException | HTTPException e) {
//            e.printStackTrace();
//        }
//
//        if (interventionList == null)
//            return;
//
//        if (BuildConfig.DEBUG)
//            Log.v(TAG, "[interventions] number : " + interventionList.size() );
//        Iterator<Intervention> interventionIterator = interventionList.iterator();
//
//        // create the lists containing items number
//        List<Integer> remoteNumbers = new ArrayList<>();
//        List<Integer> localNumbers = getLocalItemsNumbers(account);
//
//        while(interventionIterator.hasNext())
//        {
//            Intervention intervention = interventionIterator.next();
//
//            // adds current remote item number to the list
//            remoteNumbers.add(intervention.getNumber());
//
//            insertIntervention(intervention, account);
//            insertInterventionParams(intervention, account, instance);
//        }
//
//        // check and delete local item if not exists on remote
//        for (Integer i : localNumbers) {
//
//            if (!remoteNumbers.contains(i)){
//                cr.delete(Interventions.CONTENT_URI,
//                Interventions.USER + " LIKE " + " \"" + account.name + "\"" + " AND " +
//                        Interventions.NUMBER + " == " + i,null);
//
//                if (BuildConfig.DEBUG)
//                    Log.d(TAG, String.format("Item number #%s deleted", i));
//            }
//        }
//
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[interventions] GET sync done.");
//    }
//
//    private List<Integer> getLocalItemsNumbers(Account account) {
//
//        try (Cursor cursor = cr.query(
//                Interventions.CONTENT_URI,
//                Interventions.PROJECTION_NUMBER,
//                Interventions.USER + " LIKE " + "\"" + account.name + "\"",
//                null,null)) {
//
//            List<Integer> localItemsNumbers = new ArrayList<>();
//
//            if (cursor != null && cursor.getCount() > 0) {
//                cursor.moveToFirst();
//
//                while (!cursor.isAfterLast()) {
//                    localItemsNumbers.add(cursor.getInt(1));
//                    cursor.moveToNext();
//                }
//            }
//            return localItemsNumbers;
//        }
//    }
//
//    private void insertInterventionParams(Intervention intervention, Account account, Instance instance)
//    {
//        ContentValues cv = new ContentValues();
//        int interventionID = getInterventionID(account, intervention.getId());
//        int i = -1;
//        int paramLength = intervention.getParamLength();
//
//        if (interventionID == 0)
//            return;
//        while (++i < paramLength)
//        {
//            try
//            {
//                cv.clear();
//                cv.put(InterventionParameters.EK_ID, intervention.getParamID(i));
//                cv.put(InterventionParameters.NAME, intervention.getParamName(i));
//                cv.put(InterventionParameters.FK_INTERVENTION, interventionID);
//                cv.put(InterventionParameters.ROLE, intervention.getParamRole(i));
//                cv.put(InterventionParameters.LABEL, intervention.getParamLabel(i));
//                cv.put(InterventionParameters.PRODUCT_NAME, intervention.getProductName(i));
//                cv.put(InterventionParameters.PRODUCT_ID, intervention.getProductID(i));
//                if (intervention.getParamRole(i).equals("target"))
//                {
//                    String json = Intervention.getGeoJson(instance, intervention.getParamID(i));
//                    if (json != null)
//                        cv.put(InterventionParameters.SHAPE, json);
//                }
//                cr.insert(InterventionParameters.CONTENT_URI, cv);
//            }
//            catch (JSONException jsonex)
//            {
//                jsonex.printStackTrace();
//            }
//        }
//    }
//
//    private int getInterventionID(Account account, int refID)
//    {
//        try (Cursor cursor = cr.query(Interventions.CONTENT_URI,
//                Interventions.PROJECTION_NONE,
//                refID + " == " + Interventions.EK_ID + " AND "
//                        + "\"" + account.name + "\"" + " LIKE " + Interventions.USER,
//                null,
//                null)) {
//            if (cursor == null || cursor.getCount() == 0)
//                return (0);
//            cursor.moveToFirst();
//            int ret = cursor.getInt(0);
//            return (ret);
//        }
//    }
//
//    private void insertIntervention(Intervention intervention, Account account)
//    {
//        ContentValues cv = new ContentValues();
//
//        cv.put(Interventions.EK_ID, intervention.getId());
//        cv.put(Interventions.NAME, intervention.getName());
//        cv.put(Interventions.TYPE, intervention.getType());
//        cv.put(Interventions.PROCEDURE_NAME, intervention.getProcedureName());
//        cv.put(Interventions.NUMBER, intervention.getNumber());
//        cv.put(Interventions.STARTED_AT, intervention.getStartedAt());
//        cv.put(Interventions.STOPPED_AT, intervention.getStoppedAt());
//        cv.put(Interventions.DESCRIPTION, intervention.getDescription());
//        cv.put(Interventions.USER, account.name);
//        if (idExists(intervention.getId(), account))
//            cr.update(Interventions.CONTENT_URI,
//                    cv,
//                    intervention.getId() + " == " + Interventions.EK_ID,
//                    null);
//        else
//        {
//            cv.put(Interventions.UUID, UUID.randomUUID().toString());
//            cr.insert(Interventions.CONTENT_URI, cv);
//        }
//    }
//
//    private boolean idExists(int refID, Account account)
//    {
//        try(Cursor curs = cr.query(
//                Interventions.CONTENT_URI,
//                Interventions.PROJECTION_NONE,
//                refID + " == " + Interventions.EK_ID + " AND "
//                + "\"" + account.name + "\"" + " LIKE " + Interventions.USER,
//                null,
//                null)) {
//            return curs != null && curs.getCount() != 0;
//        }
//    }
//
//    private void pushIntervention(Account account) {
//
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[interventions] POST sync started...");
//
//        sendProgressNotification("Synchronisation des interventions");
//
//        try(Cursor cursorIntervention = cr.query(Interventions.CONTENT_URI, Interventions.PROJECTION_POST,
//                Interventions.USER + " LIKE ? AND (" +
//                        Interventions.STATE + " LIKE ? OR " +
//                        Interventions.STATE + " LIKE ? OR " +
//                        Interventions.STATE + " LIKE ?)",
//                new String[]{account.name, InterventionActivity.STATUS_FINISHED,
//                        InterventionActivity.STATUS_PAUSE, InterventionActivity.STATUS_IN_PROGRESS},
//                Interventions.SORT_ORDER_DEFAULT)) {
//
//            if (cursorIntervention != null && cursorIntervention.getCount() > 0) {
//                while (cursorIntervention.moveToNext()) {
//                    try (Cursor cursorWorkingPeriods = cr.query(WorkingPeriods.CONTENT_URI,
//                            WorkingPeriods.PROJECTION_ALL,
//                            WorkingPeriods.FK_INTERVENTION + " = " + cursorIntervention.getInt(0),
//                            null, null)) {
//
//                        postNewIntervention(cursorIntervention, cursorWorkingPeriods, instance);
//                    } catch (JSONException | IOException | HTTPException e) {
//                        e.printStackTrace();
//                    }
//                }
//            } else {
//                if (BuildConfig.DEBUG)
//                    Log.v(TAG, "[interventions] POST nothing to sync.");
//            }
//        }
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[interventions] POST sync done.");
//    }
//
//    private void postNewIntervention(Cursor cursorIntervention, Cursor cursorWorkingPeriods, Instance instance)
//            throws JSONException, IOException, HTTPException {
//
//        if (BuildConfig.DEBUG)
//            Log.v(TAG, "[interventions] POST new one.");
//
//        // Post it to ekylibre
//        JSONObject attributes = new JSONObject();
//        if (cursorIntervention.getInt(1) > 0) {
//            attributes.put("intervention_id", cursorIntervention.getInt(1));  // "request_"
//            attributes.put("request_compliant", cursorIntervention.getInt(3));
//        }
//        attributes.put("uuid", cursorIntervention.getString(5));
//        if (cursorIntervention.getString(4).equals(InterventionActivity.STATUS_FINISHED)) {
//            attributes.put("state", "done");
//            // Adds hour meter for equipments
//            JSONArray hourMeters = createHourMeters(cursorIntervention.getInt(0));
//            if (hourMeters.length() > 0)
//                attributes.put("equipments", hourMeters);
//        }
//        else
//            attributes.put("state", "in_progress");
//
//        attributes.put("procedure_name", cursorIntervention.getString(2));
//        attributes.put("device_uid", "android:" + Secure.getString(cr, Secure.ANDROID_ID));
//
//        attributes.put("working_periods", createWorkingPeriods(cursorIntervention.getInt(0)));
//        attributes.put("crumbs", createCrumbs(cursorIntervention.getInt(0)));
//
//        JSONObject provider = new JSONObject();
//        provider.put("zero_id", cursorIntervention.getInt(1));
//        attributes.put("providers", provider);
//
//        if (!cursorIntervention.isNull(6))
//            attributes.put("description", cursorIntervention.getString(6));
//
//        long id = Intervention.create(instance, attributes);
//        // Marks them as synced
//        //ContentValues values = new ContentValues();
//        //values.put(Interventions.STATE, "SYNCED");
//
//        //cr.update(Uri.withAppendedPath(Interventions.CONTENT_URI, Long
//        //        .toString(cursorIntervention.getLong(0))), values, null, null);
//    }
//
//    private JSONArray createHourMeters(int idIntervention) throws JSONException {
//
//        String[] args = new String[]{String.valueOf(idIntervention)};
//        JSONArray array = new JSONArray();
//
//        try (Cursor curs = cr.query(InterventionParameters.CONTENT_URI,
//                InterventionParameters.PROJECTION_TOOL_FULL,
//                "fk_intervention = ? AND hour_meter IS NOT NULL", args, null)) {
//
//            while (curs != null && curs.moveToNext()) {
//                JSONObject obj = new JSONObject();
//                obj.put("product_id", curs.getInt(4));
//                obj.put("hour_counter", curs.getFloat(5));
//                array.put(obj);
//            }
//        }
//
//        return array;
//    }
//
//    private JSONArray createCrumbs(int idIntervention) {
//
//        try (Cursor cursor = cr.query(Crumbs.CONTENT_URI,
//                Crumbs.PROJECTION_ALL,
//                Crumbs.FK_INTERVENTION + " == " + idIntervention,
//                null, Crumbs.SORT_ORDER_DEFAULT)) {
//            if (cursor != null && cursor.getCount() > 0) {
//                JSONArray crumbsArray = new JSONArray();
//                while (cursor.moveToNext()) {
//                    crumbsArray.put(addNewCrumb(cursor));
//                }
//                return (crumbsArray);
//            } else if (BuildConfig.DEBUG)
//                Log.v(TAG, "[interventions] POST no crumbs.");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return (null);
//    }
//
//    private JSONObject addNewCrumb(Cursor cursor) throws JSONException {
//        // Post it to ekylibre
//        JSONObject attributes = new JSONObject();
//        attributes.put("nature", cursor.getString(1));
//        attributes.put("geolocation", "SRID=4326; POINT(" + cursor.getDouble(3) + " " + cursor.getDouble(2) + ")");
//        attributes.put("read_at", iso8601.format(new Date(cursor.getLong(4))));
//        attributes.put("accuracy", cursor.getString(5));
//        JSONObject hash = new JSONObject();
//        Uri metadata = Uri.parse("http://domain.tld?" + cursor.getString(6));
//        Set<String> keys = metadata.getQueryParameterNames();
//        if (keys.size() > 0) {
//            for (String key : keys) {
//                if (!key.equals("null"))
//                    hash.put(key, metadata.getQueryParameter(key));
//            }
//            if (hash.length() > 0)
//                attributes.put("metadata", hash);
//        }
//        return attributes;
//    }
//
//    private JSONArray createWorkingPeriods(int interventionID) {
//
//        try (Cursor cursor = cr.query(WorkingPeriods.CONTENT_URI,
//                WorkingPeriods.PROJECTION_POST,
//                interventionID + " == " + WorkingPeriods.FK_INTERVENTION,
//                null,
//                PlantCountingItems.SORT_ORDER_DEFAULT)) {
//
//            if (cursor != null && cursor.getCount() > 0) {
//
//                JSONArray arrayJSON = new JSONArray();
//
//                while (cursor.moveToNext()) {
//                    JSONObject attributes = new JSONObject();
//                    attributes.put("started_at", cursor.getString(1));
//                    attributes.put("stopped_at", cursor.getString(2));
//                    attributes.put("nature", cursor.getString(3));
//                    arrayJSON.put(attributes);
//                }
//                return (arrayJSON);
//            }
//            else {
//                if (BuildConfig.DEBUG)
//                    Log.v(TAG, "[interventions] POST no working periods.");
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return (null);
//    }
//
//    private void pullContacts(Account account)
//    {
//        if (!getContactPref()) {
//            return;
//        }
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[contact] sync started...");
//
//        ContentValues cv = new ContentValues();
//
//        List<ContactCaller> contactsList;
//        String lastSyncContact = null;
//        try
//        {
//            lastSyncContact = getLastSyncContact(account);
//        } catch (UnsupportedEncodingException e)
//        {
//            lastSyncContact = "";
//            e.printStackTrace();
//        }
//        contactsList = ContactCaller.all(instance, lastSyncContact);
//
//
//
//        if (contactsList == null)
//            return;
//        Contact contactCreator = new Contact(context);
//        for (ContactCaller contact : contactsList)
//        {
//            cv.clear();
//            contactCreator.clear();
//            if (contact.getType().equals("create"))
//                insertContact(account, contact, contactCreator, instance, cv);
//            else if (contact.getType().equals("update"))
//            {
//                destroyContact(contact, contactCreator);
//                updateContact(account, contact, contactCreator, instance, cv);
//            }
//            else
//            {
//                destroyContact(contact, contactCreator);
//                cr.delete(Contacts.CONTENT_URI,
//                        contact.getEkId() + " == " + Contacts.EK_ID,
//                        null);
//            }
//        }
//        setNewSyncDate(account);
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[contact] sync done.");
//    }
//
//    private void destroyContact(ContactCaller contact, Contact contactCreator) {
//        Log.d(TAG, "[contact] try to destroy id => " + contact.getEkId());
//        try (Cursor curs = cr.query(
//                Contacts.CONTENT_URI,
//                Contacts.PROJECTION_NAME,
//                Contacts.EK_ID + " == " + contact.getEkId(),
//                null,
//                null)) {
//            if (curs == null || curs.getCount() == 0)
//                return;
//            curs.moveToFirst();
//            contactCreator.deleteContact(curs.getString(0), curs.getString(1), context);
//            contactCreator.commit();
//            contactCreator.clear();
//        }
//    }
//
//    private void insertContact(Account account, ContactCaller contact, Contact contactCreator,
//                               Instance instance, ContentValues cv)
//    {
//        String picture;
//
//
//        cv.put(Contacts.LAST_NAME, contact.getLastName());
//        cv.put(Contacts.FIRST_NAME, contact.getFirstName());
//        cv.put(Contacts.PICTURE_ID, contact.getPictureId());
//        cv.put(Contacts.USER, account.name);
//        cv.put(Contacts.EK_ID, contact.getEkId());
//        cv.put(Contacts.TYPE, contact.getType());
//        contactCreator.setAccount(account);
//        if (contact.getPictureId() > 0)
//        {
//            picture = contact.getPicture(instance, contact.getPictureId());
//            cv.put(Contacts.PICTURE, picture);
//            if (picture != null)
//                contactCreator.setPhoto(picture);
//        }
//        contactCreator.setName(contact.getFirstName(), contact.getLastName());
//        contactCreator.setOrganization(contact.getOrganizationName(), contact.getOrganizationPost());
//        addContactParams(contact, contactCreator);
//        cr.insert(Contacts.CONTENT_URI, cv);
//        contactCreator.commit();
//    }
//
//    private void updateContact(Account account, ContactCaller contact, Contact contactCreator,
//                               Instance instance, ContentValues cv)
//    {
//        String picture;
//
//
//        cv.put(Contacts.LAST_NAME, contact.getLastName());
//        cv.put(Contacts.FIRST_NAME, contact.getFirstName());
//        cv.put(Contacts.PICTURE_ID, contact.getPictureId());
//        cv.put(Contacts.USER, account.name);
//        cv.put(Contacts.EK_ID, contact.getEkId());
//        cv.put(Contacts.TYPE, contact.getType());
//        contactCreator.setAccount(account);
//        if (contact.getPictureId() > 0)
//        {
//            picture = contact.getPicture(instance, contact.getPictureId());
//            cv.put(Contacts.PICTURE, picture);
//            if (picture != null)
//                contactCreator.setPhoto(picture);
//        }
//        contactCreator.setName(contact.getFirstName(), contact.getLastName());
//        contactCreator.setOrganization(contact.getOrganizationName(), contact.getOrganizationPost());
//
//        updateContactParams(contact, contactCreator);
//
//        cr.update(Contacts.CONTENT_URI,
//                cv,
//                contact.getEkId() + " == " + Contacts.EK_ID,
//                null);
//        contactCreator.commit();
//    }
//
//    private void updateContactParams(ContactCaller contact, Contact contactCreator)
//    {
//        try (Cursor curs = cr.query(
//                Contacts.CONTENT_URI,
//                Contacts.PROJECTION_NONE,
//                Contacts.EK_ID + " == " + contact.getEkId(),
//                null,
//                null)) {
//            if (curs != null && curs.getCount() != 0) {
//                curs.moveToNext();
//                cr.delete(ContactParams.CONTENT_URI,
//                        ContactParams.FK_CONTACT + " == " + curs.getInt(0),
//                        null);
//            }
//            addContactParams(contact, contactCreator);
//        }
//    }
//
//
//    private void setNewSyncDate(Account account)
//    {
//        ContentValues cv = new ContentValues();
//        cv.put(LastSyncs.DATE, DateConstant.getCurrentDateFormatted());
//        int ret = cr.update(
//                LastSyncs.CONTENT_URI,
//                cv,
//                LastSyncs.USER + " LIKE " + "\"" + account.name + "\"" +
//                        " AND " + LastSyncs.TYPE + " LIKE " + "\"CONTACT\"",
//                null);
//
//        if (ret == 0)
//        {
//            cv.put(LastSyncs.USER, account.name);
//            cv.put(LastSyncs.TYPE, "CONTACT");
//            cr.insert(LastSyncs.CONTENT_URI, cv);
//        }
//    }
//
//    private String getLastSyncContact(Account account) throws UnsupportedEncodingException
//    {
//        try (Cursor curs = cr.query(
//                LastSyncs.CONTENT_URI,
//                LastSyncs.PROJECTION_DATE,
//                LastSyncs.USER + " LIKE " + "\"" + account.name + "\"",
//                null,
//                null)) {
//            if (curs == null || curs.getCount() == 0)
//                return ("");
//            curs.moveToFirst();
//            String ret = curs.getString(0);
//            ret = "?last_synchro=" + URLEncoder.encode(ret, StandardCharsets.UTF_8.name());
//            return (ret);
//        }
//    }
//
//    private void addContactParams(ContactCaller contact, Contact contactCreator)
//    {
//        ContentValues cv = new ContentValues();
//        int i = -1;
//
//        while (contact.getMailLines(++i) != null)
//        {
//            cv.clear();
//            cv.put(ContactParams.MAIL_LINES, contact.getMailLines(i));
//            cv.put(ContactParams.POSTAL_CODE, contact.getPostalCode(i));
//            cv.put(ContactParams.CITY, contact.getCity(i));
//            cv.put(ContactParams.COUNTRY, contact.getCountry(i));
//            cv.put(ContactParams.TYPE, Contact.TYPE_MAIL);
//            contactCreator.setMail(contact.getMailLines(i), contact.getPostalCode(i),
//                    contact.getCity(i), contact.getCountry(i));
//            cr.insert(ContactParams.CONTENT_URI, cv);
//        }
//        String email;
//        while ((email = contact.getNextEmail()) != null)
//        {
//            cv.clear();
//            cv.put(ContactParams.EMAIL, email);
//            cv.put(ContactParams.TYPE, Contact.TYPE_EMAIL);
//            contactCreator.setEmail(email);
//            cr.insert(ContactParams.CONTENT_URI, cv);
//        }
//        String mobile;
//        while ((mobile = contact.getNextMobile()) != null)
//        {
//            cv.clear();
//            cv.put(ContactParams.MOBILE, mobile);
//            cv.put(ContactParams.TYPE, Contact.TYPE_MOBILE);
//            contactCreator.setMobileNumber(mobile);
//            cr.insert(ContactParams.CONTENT_URI, cv);
//        }
//        String phone;
//        while ((phone = contact.getNextPhone()) != null)
//        {
//            cv.clear();
//            cv.put(ContactParams.PHONE, phone);
//            cv.put(ContactParams.TYPE, Contact.TYPE_PHONE);
//            contactCreator.setWorkNumber(phone);
//            cr.insert(ContactParams.CONTENT_URI, cv);
//        }
//        String website;
//        while ((website = contact.getNextWebsite()) != null)
//        {
//            cv.clear();
//            cv.put(ContactParams.WEBSITE, website);
//            cv.put(ContactParams.TYPE, Contact.TYPE_WEBSITE);
//            contactCreator.setWebsite(website);
//            cr.insert(ContactParams.CONTENT_URI, cv);
//        }
//    }
//
//    private void pushObservation(Account account) {
//
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[observation] sync started...");
//
//        sendProgressNotification("Synchronisation des observations");
//
//        // Get all non-synced observations
//        try (Cursor cursor = cr.query(Observations.CONTENT_URI, Observations.PROJECTION_ALL,
//                Observations.USER + " LIKE " + "\"" + account.name + "\"" +
//                        " AND " + Observations.SYNCED + " == " + 0, null, Observations.SORT_ORDER_DEFAULT)) {
//
//            if (cursor != null && cursor.getCount() > 0) {
//                while (cursor.moveToNext()) {
//                    try (Cursor issuesCursor = cr.query(
//                            ObservationIssues.CONTENT_URI,
//                            ObservationIssues.PROJECTION_ALL,
//                            ObservationIssues.FK_OBSERVATION + " == " +
//                                    cursor.getInt(0), null, null)) {
//                        try {
//                            postNewObservation(cursor, issuesCursor, instance);
//                        } catch (JSONException | IOException | HTTPException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            } else if (BuildConfig.DEBUG)
//                Log.v(TAG, "[observation] nothing to sync.");
//        }
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[observation] sync done.");
//
//    }
//
//    private void postNewObservation(Cursor observationCursor, Cursor issueCursor, Instance instance) throws JSONException, IOException, HTTPException {
//
//        if (BuildConfig.DEBUG)
//            Log.v(TAG, "[observation] post new one.");
//        // Creates JSONObject for each observation
//        JSONObject attributes = new JSONObject();
//        SimpleDateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);
//        attributes.put("observed_on", dateTime.format(new Date(observationCursor.getLong(2))));
//        attributes.put("activity_id", observationCursor.getInt(1));
//        if (!observationCursor.isNull(4))
//            attributes.put("scale_id", observationCursor.getInt(4));
//        if (!observationCursor.isNull(6))
//            attributes.put("description", observationCursor.getString(6));
//        // Parse geolocation
//        if (!observationCursor.isNull(7) && !observationCursor.isNull(8))
//            attributes.put("geolocation", String.format("SRID=4326; POINT(%s %s)",
//                    observationCursor.getDouble(7), observationCursor.getDouble(8)));
//
//        // Parse pictures
//        if (!observationCursor.isNull(5)) {
//            String[] paths = observationCursor.getString(5).split(",");
//            Log.e(TAG, "there is " + paths.length + " pictures");
//
//            JSONArray jsonArray = new JSONArray();
//            for (String path : paths) {
//
//                String filePath = null;
//                Uri pictureUri = Uri.parse(path);
//
//                if (Objects.equals(pictureUri.getScheme(), "content")) {
//
//                    String wholeID = DocumentsContract.getDocumentId(pictureUri);
//                    String id = wholeID.split(":")[1];
//                    String[] column = {MediaStore.Images.Media.DATA};
//                    String selection = MediaStore.Images.Media._ID + "=?";
//
//                    // TODO check if MediaStore.Images.Media.INTERNAL_CONTENT_URI is needed
//                    try (Cursor cursor = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                            column, selection, new String[]{id}, null)) {
//
//                        if (cursor != null) {
//                            cursor.moveToFirst();
//                            filePath = "file://" + cursor.getString(0);
//                        }
//                    }
//                } else {
//                    filePath = pictureUri.toString();
//                }
//
//                Uri uri = Uri.parse(filePath);
//
//                Log.e(TAG, "Uri = " + uri);
//
//                // TODO -> do not crop image and rotate according to exif
//
//                InputStream is = getContext().getContentResolver().openInputStream(uri);
//                Bitmap original = BitmapFactory.decodeStream(is);
//
//                Bitmap rotated = ImageConverter.rotateImage(original, uri);
//                Bitmap bitmap= ImageConverter.resizeImage(rotated, 2000);
//
//                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
//                byte[] byteArrayImage = byteArrayOutputStream.toByteArray();
//                String pictureJson = "data:image/jpeg;base64," + Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
//                jsonArray.put(pictureJson);
//            }
//            attributes.put("pictures", jsonArray);
//        }
//
//        // Parse plants
//        if (!observationCursor.isNull(3)) {
//            String[] plants = observationCursor.getString(3).split(",");
//            JSONArray jsonArray = new JSONArray();
//            for (String plant : plants) {
//                JSONObject jsonObject = new JSONObject();
//                jsonObject.put("id", plant);
//                jsonArray.put(jsonObject);
//            }
//            if (jsonArray.length() > 0)
//                attributes.put("plants", jsonArray);
//        }
//
//        // Parse issues
//        if (issueCursor != null) {
//            JSONArray jsonArray = new JSONArray();
//            try {
//                while (issueCursor.moveToNext()) {
//                    if (!issueCursor.isNull(3)) {
//                        JSONObject jsonObject = new JSONObject();
//                        jsonObject.put("id", issueCursor.getInt(3));
//                        jsonArray.put(jsonObject);
//                    }
//                }
//            } finally {
//                issueCursor.close();
//            }
//            if (jsonArray.length() > 0)
//                attributes.put("issues", jsonArray);
//        }
//
//
//        long resultId = Observation.create(instance, attributes);
//        Log.e(TAG, "Obs #" + resultId +" created !" );
//        // Marks them as synced
//        ContentValues cv = new ContentValues();
//        cv.put(Observations.SYNCED, 1);
//        cr.update(Uri.withAppendedPath(Observations.CONTENT_URI, Long.toString(observationCursor.getLong(0))), cv, null, null);
//        if (BuildConfig.DEBUG)
//            Log.v(TAG, "[observation] post done.");
//    }
//
//    /**
//     *   The main request for products
//     */
//    @SuppressLint("ApplySharedPref")
//    private void pullProducts(Account account) {
//
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[products] sync started...");
//
//        SQLiteDatabase db = new DatabaseHelper(getContext()).getReadableDatabase();
//        String[] productTypes = new String[]{"equipment", "land_parcel", "matter", "plant", "worker", "building_division"};
//
//        String currentAccount = AccountTool.getInstance(account);
//
//        for (String type : productTypes) {
//
//            String message = String.format("Synchronisation des %ss", getTranslation(type).toLowerCase());
//            sendProgressNotification(message);
//
//            List<Product> productList = null;
//
//            try {
//                // Check in database there is products, if not, do a full sync
//                long count = DatabaseUtils.queryNumEntries(db,
//                        "products", "variety = ? AND user = ?",
//                        new String[]{type, currentAccount});
//
//                // Do the API call
//                productList = Product.all(instance, count == 0 ? null : lastSyncattribute, type);
//
//            } catch (JSONException | IOException | HTTPException | ParseException e) {
//                Log.e(TAG, "Failed to load PRODUCTS -> " + e.getMessage());
////                error = true;
//            }
//
//            if (productList == null)
//                continue;
//
//            if (BuildConfig.DEBUG)
//                Log.v(TAG, "[products] there is " + productList.size() + " " + type);
//
////            List<ContentValues> bulkProductsCV = new ArrayList<>();
//            ArrayList<ContentProviderOperation> operations = new ArrayList<>();
//
//            int maxCount = productList.size();
//
//            int index = 0;
//            for (Product product : productList) {
//
//                ContentValues cv = new ContentValues();
//                cv.put(Products.EK_ID, product.id);
//                cv.put(Products.NAME, product.name);
//                cv.put(Products.SEARCH_NAME, StringUtils.stripAccents(product.name).toLowerCase());
//                cv.put(Products.VARIETY, product.variety);
//                cv.put(Products.ABILITIES, product.abilities);
//                cv.put(Products.NUMBER, product.number);
//                cv.put(Products.WORK_NUMBER, product.workNumber);
//                cv.put(Products.POPULATION, product.population);
//                cv.put(Products.UNIT, product.unit);
//                cv.put(Products.CONTAINER_NAME, product.containerName);
//                if (product.deadAt != null)
//                    cv.put(Products.DEAD_AT, product.deadAt.getTime());
//                cv.put(Products.NET_SURFACE_AREA, product.netSurfaceArea);
////                if (product.production_started_on != null)
////                    cv.put(Products.PRODUCTION_STARTED_ON, product.production_started_on.getTime());
////                if (product.production_stopped_on != null)
////                    cv.put(Products.PRODUCTION_STOPPED_ON, product.production_stopped_on.getTime());
//                cv.put(Products.HAS_HOUR_COUNTER, product.hasHourCounter);
//                cv.put(Products.USER, currentAccount);
//
//                operations.add(ContentProviderOperation.newInsert(Products.CONTENT_URI).withValues(cv).build());
//
//                // Bulk insert
//                if (++index % 50 == 0)
//                    bulkInsert(operations, index, maxCount);
//            }
//
//            // Bulk insert lasting items
//            bulkInsert(operations, index, maxCount);
//
//            onlyProducts = false;
//            prefs.edit().putBoolean(AccountTool.getInstance(account) + "_productsSynced", true).commit();
//
//            if (BuildConfig.DEBUG) {
//                Log.d(TAG, "productsSynced -> " + prefs.getBoolean("productsSynced", false));
//                Log.d(TAG, "[products] sync done...");
//            }
//        }
//    }
//
//    private void bulkInsert(ArrayList<ContentProviderOperation> ops, int index, int max) {
//        Log.v(TAG, "Progess -> " + index + " ...");
//        sendProgressNotification(index, max);
//        try {
//            cr.applyBatch(AUTHORITY, ops);
//        } catch (OperationApplicationException | RemoteException e) {
//            e.printStackTrace();
//        }
//        ops.clear();
//    }
//
//    /**
//     *   The main request for variants
//     */
//    @SuppressLint("ApplySharedPref")
//    private void pullVariants(Account account) {
//
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[variants] sync started...");
//
//        sendProgressNotification("Synchronisation des variantes");
//        SQLiteDatabase db = new DatabaseHelper(getContext()).getReadableDatabase();
//
//        List<Variant> variantList = null;
//
//        String currentAccount = AccountTool.getInstance(account);
//
//        try {
//            long count = DatabaseUtils.queryNumEntries(db,"variants","user = ?",
//                    new String[] {AccountTool.getInstance(account)});
//            variantList = Variant.all(instance, count == 0 ? null : lastSyncattribute);
//        } catch (JSONException | IOException | HTTPException e) {
//            e.printStackTrace();
//        }
//
//        if (variantList == null)
//            return;
//
//        if (BuildConfig.DEBUG)
//            Log.v(TAG, "[variants] there is " + variantList.size() + " variants." );
//
////        List<ContentValues> bulkVariantsCV = new ArrayList<>();
//        ArrayList<ContentProviderOperation> operations = new ArrayList<>();
//
//        int maxCount = variantList.size();
//
//        int index = 0;
//        for (Variant variant : variantList) {
//            ContentValues cv = new ContentValues();
//            cv.put(Variants.EK_ID, variant.id);
//            cv.put(Variants.NAME, variant.name);
//            cv.put(Variants.SEARCH_NAME, StringUtils.stripAccents(variant.name).toLowerCase());
//            cv.put(Variants.VARIETY, variant.variety);
//            cv.put(Variants.ABILITIES, variant.abilities);
//            cv.put(Variants.NUMBER, variant.number);
//            cv.put(Variants.USER, currentAccount);
//
//            operations.add(ContentProviderOperation.newInsert(Variants.CONTENT_URI).withValues(cv).build());
//
//            // Bulk insert every 100 items
//            if (++index % 50 == 0)
//                bulkInsert(operations, index, maxCount);
//        }
//
//        // Bulk insert lasting items
//        bulkInsert(operations, index, maxCount);
//
//        onlyVariants = false;
//        prefs.edit().putBoolean(AccountTool.getInstance(account) + "_variantsSynced", true).commit();
//
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[variants] sync done.");
//    }
//
//    /**
//     * Push detailed intervention to Ekylibre
//     * @param account the current account
//     */
//    private void pushDetailedIntervention(Account account) {
//
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[detailed_intervention] sync started...");
//
//        sendProgressNotification("Synchronisation des nouvelles interventions");
//
//        // Get interventions that are not yet created in Ekylibre (no ek_id)
//        final String whereClause = "user LIKE ? AND ek_id IS NULL AND status = ?";
//        final String[] args = new String[] {AccountTool.getInstance(account), FINISHED};
//
//        // Get all non-synced observations
//        try (Cursor cursor = cr.query(DetailedInterventions.CONTENT_URI,
//                DetailedInterventions.PROJECTION_ALL, whereClause, args,
//                DetailedInterventions.ORDER_BY_ID_DESC)) {
//
//            // Projection --> _ID, PROCEDURE_NAME
//
//            // Loop over all selected interventions
//            while (cursor != null && cursor.moveToNext()) {
//
//                // Create new DetailedIntervention payload
//                JSONObject payload = new JSONObject();
//                JSONObject provider = new JSONObject();
//                provider.put("zero_id", cursor.getInt(0));
//                payload.put("providers", provider);
//                payload.put("procedure_name", cursor.getString(1));
//
//                if (!cursor.isNull(2)) {
//                    JSONArray array = new JSONArray(cursor.getString(2).split(","));
//                    payload.put("actions", array);
//                }
//
//                if (!cursor.isNull(4)) {
//                    payload.put("description", cursor.getString(4));
//                }
//
//                // Prepare InterventionAttributes query
//                final String whereId = "detailed_intervention_id = ?";
//                final String whereIdAndNatureIsNull = "detailed_intervention_id = ? AND nature IS NULL";
//                final String[] id = new String[] {cursor.getString(0)};  // The current id
//
//                // --------------- //
//                // Working periods //
//                // --------------- //
//
//                try (Cursor cursPeriod = cr.query(WorkingPeriodAttributes.CONTENT_URI,
//                        WorkingPeriodAttributes.PROJECTION_ALL, whereIdAndNatureIsNull, id, null, null)) {
//
//                    // Create JSON array for working periods
//                    JSONArray workingPeriods = new JSONArray();
//
//                    while (cursPeriod != null && cursPeriod.moveToNext()) {
//                        JSONObject period = new JSONObject();
////                        period.put("started_at", iso8601Print.print(new DateTime(new Date(cursPeriod.getInt(0)))));
////                        Log.e(TAG, "Started at iso8601 -> " + period.getString("started_at"));
////                        period.put("stopped_at", iso8601Print.print(new DateTime(new Date(cursPeriod.getInt(1)))));
//
//                        period.put("started_at", simpleISO8601.format(new Date(cursPeriod.getLong(0))));
//                        period.put("stopped_at", simpleISO8601.format(new Date(cursPeriod.getLong(1))));
//
////                        DateTimeFormatter iso = ISODateTimeFormat.dateTime();
////                        DateTimeZone dtZone = DateTimeZone.forID("Europe/Paris");
//
////                        LocalDateTime dt1 = new LocalDateTime(cursPeriod.getLong(0)); //.withZone(dtZone)
////                        String started_at = iso.print(dt1);
////                        LocalDateTime dt2 = new LocalDateTime(cursPeriod.getLong(1));
////                        String stopped_at = iso.print(dt2);
//
////                        period.put("started_at", started_at);
////                        period.put("stopped_at", stopped_at);
//
//                        workingPeriods.put(period);
//                    }
//
//                    // Add working periods to main payload
//                    payload.put("working_periods_attributes", workingPeriods);
//                }
//
//                // --------------------- //
//                // Group zone attributes //
//                // --------------------- //
//                JSONArray zoneArray = new JSONArray();
//                try (Cursor cursZone = cr.query(GroupZones.CONTENT_URI,
//                        GroupZones.PROJECTION_ALL, whereId, id, null, null)) {
//
//                    while (cursZone != null && cursZone.moveToNext()) {
//                        JSONObject zone = new JSONObject();
//                        zone.put("reference_name", "zone");
//
//                        // Get target
//                        try (Cursor targetCurs = cr.query(DetailedInterventionAttributes.CONTENT_URI,
//                                DetailedInterventionAttributes.PROJECTION_ALL, "_id = ?",
//                                new String[] {cursZone.getString(1)}, null, null)) {
//
//                            JSONArray targetObjectList = new JSONArray();
//
//                            while (targetCurs != null && targetCurs.moveToNext()) {
//                                targetObjectList.put(composeJSONObject(targetCurs));
//                                zone.put("targets_attributes", targetObjectList);
//                            }
//                        }
//
//                        // Get output
//                        try (Cursor outputCurs = cr.query(DetailedInterventionAttributes.CONTENT_URI,
//                                DetailedInterventionAttributes.PROJECTION_ALL, "_id = ?",
//                                new String[] {cursZone.getString(2)}, null, null)) {
//
//                            JSONArray outputObjectList = new JSONArray();
//
//                            while (outputCurs != null && outputCurs.moveToNext()) {
//                                JSONObject outputObject = new JSONObject();
//                                outputObject.put("variant_id", outputCurs.getInt(1));
//                                outputObject.put("reference_name", outputCurs.getString(2));
//                                outputObject.put("variety", cursZone.getString(3));
//                                outputObject.put("batch_number", cursZone.getString(4));
//                                outputObjectList.put(outputObject);
//                                zone.put("outputs_attributes", outputObjectList);
//                            }
//                        }
//
//                        // Add created zone to list
//                        zoneArray.put(zone);
//                    }
//                }
//
//                // --------------------- //
//                // Group work attributes //
//                // --------------------- //
//                JSONArray workArray = new JSONArray();
//                try (Cursor cursWork = cr.query(GroupWorks.CONTENT_URI,
//                        GroupWorks.PROJECTION_ALL, whereId, id, null, null)) {
//
//                    while (cursWork != null && cursWork.moveToNext()) {
//                        JSONObject work = new JSONObject();
//                        work.put("reference_name", "work");
//
//                        // Get target
//                        try (Cursor targetCurs = cr.query(DetailedInterventionAttributes.CONTENT_URI,
//                                DetailedInterventionAttributes.PROJECTION_ALL, "_id = ?",
//                                new String[] {cursWork.getString(1)}, null, null)) {
//
//                            JSONArray targetObjectList = new JSONArray();
//
//                            while (targetCurs != null && targetCurs.moveToNext()) {
//                                targetObjectList.put(composeJSONObject(targetCurs));
//                                work.put("targets_attributes", targetObjectList);
//                            }
//                        }
//
//                        // Get inputs
//                        String[] inputIds = cursWork.getString(2).split(",");
//                        JSONArray inputObjectList = new JSONArray();
//                        for (String inputId : inputIds) {
//                            try (Cursor inputCurs = cr.query(DetailedInterventionAttributes.CONTENT_URI,
//                                    DetailedInterventionAttributes.PROJECTION_ALL, "_id = ?",
//                                    new String[]{inputId}, null, null)) {
//
//                                while (inputCurs != null && inputCurs.moveToNext()) {
//                                    inputObjectList.put(composeQuantityJSONObject(inputCurs, "product_id"));
//                                }
//                            }
//                        }
//                        // Add all inputs
//                        work.put("inputs_attributes", inputObjectList);
//
//                        // Add created zone to list
//                        workArray.put(work);
//                    }
//                }
//
//                // ----------------------- //
//                // Intervention attributes //
//                // ----------------------- //
//                try (Cursor cursAttrs = cr.query(DetailedInterventionAttributes.CONTENT_URI,
//                        DetailedInterventionAttributes.PROJECTION_ALL, whereId, id, null, null)) {
//
//                    // Projection --> ROLE, REFERENCE_ID, REFERENCE_NAME, QUANTITY_VALUE, QUANTITY_UNIT_NAME
//
//                    JSONArray workersArray = new JSONArray();
//                    JSONArray targetsArray = new JSONArray();
//                    JSONArray equipmentsArray = new JSONArray();
//                    JSONArray inputsArray = new JSONArray();
//                    JSONArray outputsArray = new JSONArray();
//
//                    while (cursAttrs != null && cursAttrs.moveToNext()) {
//                        String role = cursAttrs.getString(0);
//                        switch (role) {
//
//                            case "workers":
//                                workersArray.put(composeJSONObject(cursAttrs));
//                                break;
//
//                            case "equipments":
//                                equipmentsArray.put(composeJSONObject(cursAttrs));
//                                break;
//
//                            case "targets":
//                                if (cursAttrs.isNull(5))
//                                    targetsArray.put(composeJSONObject(cursAttrs));
//                                break;
//
//                            case "inputs":
//                                if (cursAttrs.isNull(5))
//                                    inputsArray.put(composeQuantityJSONObject(cursAttrs, "product_id"));
//                                break;
//
//                            case "outputs":
//                                if (cursAttrs.isNull(5))
//                                    outputsArray.put(composeQuantityJSONObject(cursAttrs, "variant_id"));
//                                break;
//                        }
//                    }
//
//                    // Add all attributs to corresponding payload value
//                    if (workersArray.length() > 0)
//                        payload.put("doers_attributes", workersArray);
//
//                    if (equipmentsArray.length() > 0)
//                        payload.put("tools_attributes", equipmentsArray);
//
//                    if (targetsArray.length() > 0)
//                        payload.put("targets_attributes", targetsArray);
//
//                    if (inputsArray.length() > 0)
//                        payload.put("inputs_attributes", inputsArray);
//
//                    if (outputsArray.length() > 0)
//                        payload.put("outputs_attributes", outputsArray);
//
//                    if (zoneArray.length() > 0)
//                        payload.put("group_parameters_attributes", zoneArray);
//                    else if (workArray.length() > 0)
//                        payload.put("group_parameters_attributes", workArray);
//                }
//
//                // Push the new DetailedIntervention
//                long resultId = DetailedIntervention.create(instance, payload);
//
//                if (BuildConfig.DEBUG )
//                    if (resultId == -1)
//                        Log.e(TAG, "[detailed_intervention] error while saving intervention...");
//                    else
//                        Log.i(TAG, "[detailed_intervention] with id #" + resultId + " created !");
//
////                if (resultId != -1) {
//
//                // Save the returning id
//                ContentValues cv = new ContentValues();
//                cv.put(DetailedInterventions.EK_ID, resultId);
//                Uri uri = Uri.withAppendedPath(DetailedInterventions.CONTENT_URI, cursor.getString(0));
//                Log.e(TAG, "uri -> " + uri);
//                cr.update(uri, cv, null, null);
//
//                // Send intervention participations if existing
//                createInterventionParticipations(cursor, resultId);
//
//                // Push Intervention Participations (timers)
////                } else
////                    if (BuildConfig.DEBUG)
////                        Log.e(TAG, "Error while saving intervention...");
//            }
//        } catch (JSONException | IOException | HTTPException e) {
//            e.printStackTrace();
//        }
//
//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "[detailed_intervention] sync done.");
//    }
//
//    @SuppressLint("HardwareIds")
//    private void createInterventionParticipations(Cursor curs, long createdId)
//            throws JSONException, IOException, HTTPException {
//
//        final String whereIdAndNatureNotNull = "detailed_intervention_id = ? AND nature IS NOT NULL";
//        final String[] id = new String[] {curs.getString(0)};  // The current intervention id
//
//        try (Cursor cursPeriod = cr.query(WorkingPeriodAttributes.CONTENT_URI,
//                WorkingPeriodAttributes.PROJECTION_ALL, whereIdAndNatureNotNull, id, null, null)) {
//
//            JSONArray workingPeriods = new JSONArray();
//
//            while (cursPeriod != null && cursPeriod.moveToNext()) {
//                JSONObject period = new JSONObject();
//                period.put("started_at", cursPeriod.getString(0));
//                period.put("stopped_at", cursPeriod.getString(1));
//                period.put("nature", cursPeriod.getString(2));
//                workingPeriods.put(period);
//            }
//
//            if (workingPeriods.length() > 0) {
//                JSONObject payload = new JSONObject();
//                payload.put("state", "done");
////                payload.put("procedure_name", curs.getString(1));
////                payload.put("device_uid", "android:" + Secure.getString(cr, Secure.ANDROID_ID));
////                payload.put("uuid", curs.getString(1));
//                payload.put("intervention_id", createdId);
//                payload.put("working_periods", workingPeriods);
//
//                // Push the new DetailedIntervention
//                long resultId = Intervention.create(instance, payload);
//            }
//        }
//    }
//
//    private JSONObject composeJSONObject(Cursor curs) throws JSONException {
//        JSONObject obj = new JSONObject();
//        obj.put("product_id", curs.getInt(1));
//        obj.put("reference_name", curs.getString(2));
//        if (!curs.isNull(6)) {
//            JSONObject readingsAttributes = new JSONObject(); //
//            readingsAttributes.put("indicator_name", "hour_counter");
//            readingsAttributes.put("measure_value_value", curs.getString(6));
//            readingsAttributes.put("measure_value_unit", "hour");
//            obj.put("readings_attributes", readingsAttributes);
//        }
//        return obj;
//    }
//
//    private JSONObject composeQuantityJSONObject(Cursor curs, String idType) throws JSONException {
//        JSONObject obj = new JSONObject();
//        obj.put(idType, curs.getInt(1));
//        obj.put("reference_name", curs.getString(2));
//        if (!curs.isNull(3))
//            obj.put("quantity_value", curs.getDouble(3));
//        if (!curs.isNull(4))
//            obj.put("quantity_handler", curs.getString(4));
//        return obj;
//    }
//
//    protected Instance getInstance(Account account) {
//        Instance instance = null;
//            try {
//                instance = new Instance(account, am);
//            } catch(AccountsException e) {
//                if (BuildConfig.DEBUG) Log.e(TAG, "Account manager or user cannot help. Cannot get token.");
//            } catch(IOException e) {
//                if (BuildConfig.DEBUG) Log.w(TAG, "IO problem. Cannot get token.");
//            }
//        return (instance);
//    }
//
//    /*
//     ** Get mobilePerm from SharedPreference set on settings activity
//     */
//    private boolean getContactPref() {
//        SharedPreferences pref;
//        pref = PreferenceManager.getDefaultSharedPreferences(context);
//        return (pref.getBoolean(SettingsActivity.PREF_SYNC_CONTACTS, false));
//    }
//
//
//}
