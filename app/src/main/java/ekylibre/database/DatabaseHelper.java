package ekylibre.database;

import android.accounts.Account;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import ekylibre.database.ZeroContract.DetailedInterventionAttributes;
import ekylibre.database.ZeroContract.DetailedInterventions;
import ekylibre.database.ZeroContract.WorkingPeriodAttributes;
import ekylibre.service.LongMigrationService;
import ekylibre.util.AccountTool;
import ekylibre.zero.BuildConfig;

import static ekylibre.database.ZeroContract.BuildingDivisions;
import static ekylibre.database.ZeroContract.ContactParams;
import static ekylibre.database.ZeroContract.Contacts;
import static ekylibre.database.ZeroContract.CrumbsColumns;
import static ekylibre.database.ZeroContract.EphyCropsets;
import static ekylibre.database.ZeroContract.Equipments;
import static ekylibre.database.ZeroContract.GroupWorks;
import static ekylibre.database.ZeroContract.GroupZones;
import static ekylibre.database.ZeroContract.Inputs;
import static ekylibre.database.ZeroContract.InterventionParametersColumns;
import static ekylibre.database.ZeroContract.Interventions;
import static ekylibre.database.ZeroContract.InterventionsColumns;
import static ekylibre.database.ZeroContract.IssueNatures;
import static ekylibre.database.ZeroContract.Issues;
import static ekylibre.database.ZeroContract.IssuesColumns;
import static ekylibre.database.ZeroContract.LandParcels;
import static ekylibre.database.ZeroContract.LastSyncs;
import static ekylibre.database.ZeroContract.ObservationIssues;
import static ekylibre.database.ZeroContract.Observations;
import static ekylibre.database.ZeroContract.Outputs;
import static ekylibre.database.ZeroContract.PhytosanitaryReferences;
import static ekylibre.database.ZeroContract.PhytosanitaryUsages;
import static ekylibre.database.ZeroContract.PlantCountingItemsColumns;
import static ekylibre.database.ZeroContract.PlantCountingsColumns;
import static ekylibre.database.ZeroContract.PlantDensityAbaciColumns;
import static ekylibre.database.ZeroContract.PlantDensityAbacusItemsColumns;
import static ekylibre.database.ZeroContract.Plants;
import static ekylibre.database.ZeroContract.PlantsColumns;
import static ekylibre.database.ZeroContract.Products;
import static ekylibre.database.ZeroContract.Variants;
import static ekylibre.database.ZeroContract.VegetalScale;
import static ekylibre.database.ZeroContract.Workers;
import static ekylibre.database.ZeroContract.WorkingPeriodsColumns;


public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 53;
    private static final String DATABASE_NAME = "zero";
    private static final int NO_VERSION = -1;
    private Context context;

    private static final String[] productTypes = new String[]{"equipment", "land_parcel", "matter", "plant", "worker", "building_division"};


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        try {
            onUpgrade(getReadableDatabase(), 52, DATABASE_VERSION);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    /*
    ** Use this to enable constraint on foreign key, but it's actually not working
    ** Crash on the onDelete dunno why => FOREIGN KEY constraint failed (code 787)
    */

/*
    @Override
    public void onOpen(SQLiteDatabase db)
    {
        super.onOpen(db);
        if (!db.isReadOnly())
        {
            db.setForeignKeyConstraintsEnabled(true);
        }
    }
*/


    /*
    ** Called on the first creation of the DB if there is no DB with the same DATABASE_NAME
    ** Parameters NO_VERSION is used to say there isn't any version at the creation
    */
    @Override
    public void onCreate(SQLiteDatabase database)
    {
        onUpgrade(database, NO_VERSION, DATABASE_VERSION);
    }

    /*
    ** Called when the newVersion is > than oldVersion
    ** Execute migration from oldVersion + 1 to DATABASE_VERSION
    ** note : +1 is applied to oldVersion to start at the next migration that is needed by current device
    */
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion)
    {
        if (BuildConfig.DEBUG)
            Log.d("DatabaseHelper", "OLD VERSION => " + oldVersion);

        SharedPreferences prefs = context.getSharedPreferences("prefs", Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = prefs.edit();

        oldVersion += 1;
        switch (oldVersion)
        {
            case 0:
            {
                database.execSQL("CREATE TABLE IF NOT EXISTS crumbs ("
                        + ZeroContract.CrumbsColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + CrumbsColumns.TYPE + " VARCHAR(32) NOT NULL"
                        + ", " + CrumbsColumns.LATITUDE + " DOUBLE NOT NULL"
                        + ", " + CrumbsColumns.LONGITUDE + " DOUBLE NOT NULL"
                        + ", " + CrumbsColumns.READ_AT + " BIGINT NOT NULL"
                        + ", " + CrumbsColumns.ACCURACY + " FLOAT"
                        + ", " + CrumbsColumns.SYNCED + " INTEGER NOT NULL DEFAULT 0"
                        + ", " + CrumbsColumns.METADATA + " TEXT"
                        + ")");
                if (newVersion == 0)
                    break;
            }
            case 1:
            {
                database.execSQL("CREATE TABLE IF NOT EXISTS issues ("
                        + IssuesColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + IssuesColumns.NATURE + " VARCHAR(192) NOT NULL"
                        + ", " + IssuesColumns.SEVERITY + " INTEGER NOT NULL DEFAULT 2"
                        + ", " + IssuesColumns.EMERGENCY + " INTEGER NOT NULL DEFAULT 2"
                        + ", " + IssuesColumns.DESCRIPTION + " TEXT"
                        + ", " + IssuesColumns.SYNCED + " BOOLEAN NOT NULL DEFAULT 0"
                        + ", " + IssuesColumns.PINNED + " BOOLEAN NOT NULL DEFAULT 0"
                        + ", " + IssuesColumns.SYNCED_AT + " DATE"
                        + ", " + IssuesColumns.OBSERVED_AT + " DATE"
                        + ", " + IssuesColumns.LATITUDE + " REAL"
                        + ", " + IssuesColumns.LONGITUDE + " REAL"
                        + ")");
                if (newVersion == 1)
                    break;
            }
            case 2:
            {
                database.execSQL("CREATE TABLE IF NOT EXISTS plant_countings ("
                        + PlantCountingsColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + PlantCountingsColumns.OBSERVED_AT + " DATE"
                        + ", " + PlantCountingsColumns.LATITUDE + " REAL"
                        + ", " + PlantCountingsColumns.LONGITUDE + " REAL"
                        + ", " + PlantCountingsColumns.OBSERVATION + " TEXT"
                        + ", " + PlantCountingsColumns.SYNCED_AT + " DATE"
                        + ", " + PlantCountingsColumns.PLANT_DENSITY_ABACUS_ITEM_ID + " INTEGER"
                        + ", " + PlantCountingsColumns.PLANT_DENSITY_ABACUS_ID + " INTEGER"
                        + ", " + PlantCountingsColumns.PLANT_ID + " INTEGER"
                        + ", FOREIGN KEY(" + PlantCountingsColumns.PLANT_DENSITY_ABACUS_ITEM_ID + ") REFERENCES " + PlantDensityAbacusItemsColumns.TABLE_NAME + "(" + PlantDensityAbacusItemsColumns._ID + ") ON DELETE CASCADE"
                        + ", FOREIGN KEY(" + PlantCountingsColumns.PLANT_DENSITY_ABACUS_ID + ") REFERENCES " + PlantDensityAbaciColumns.TABLE_NAME + "(" + PlantDensityAbaciColumns._ID + ") ON DELETE CASCADE"
                        + ", FOREIGN KEY(" + PlantCountingsColumns.PLANT_ID + ") REFERENCES " + PlantsColumns.TABLE_NAME + "(" + PlantsColumns._ID + ") ON DELETE CASCADE"
                        + ")");


                database.execSQL("CREATE TABLE IF NOT EXISTS plant_counting_items ("
                        + PlantCountingItemsColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + PlantCountingItemsColumns.VALUE + " INTEGER"
                        + ", " + PlantCountingItemsColumns.PLANT_COUNTING_ID + " INTEGER"
                        + ", FOREIGN KEY(" + PlantCountingItemsColumns.PLANT_COUNTING_ID + ") REFERENCES " + PlantCountingsColumns.TABLE_NAME + "(" + PlantCountingsColumns._ID + ") ON DELETE CASCADE"
                        + ")");
                if (newVersion == 2)
                    break;
            }
            case 3:
            {
                database.execSQL("CREATE TABLE IF NOT EXISTS plant_density_abaci ("
                        + PlantDensityAbaciColumns._ID + " INTEGER PRIMARY KEY"
                        + ", " + PlantDensityAbaciColumns.NAME + " VARCHAR(255)"
                        + ", " + PlantDensityAbaciColumns.VARIETY + " VARCHAR(255)"
                        + ", " + PlantDensityAbaciColumns.GERMINATION_PERCENTAGE + " REAL"
                        + ", " + PlantDensityAbaciColumns.SAMPLING_LENGTH_UNIT + " VARCHAR(255)"
                        + ", " + PlantDensityAbaciColumns.SEEDING_DENSITY_UNIT + " VARCHAR(255)"
                        + ")");

                database.execSQL("CREATE TABLE IF NOT EXISTS plant_density_abacus_items ("
                        + PlantDensityAbacusItemsColumns._ID + " INTEGER PRIMARY KEY"
                        + ", " + PlantDensityAbacusItemsColumns.SEEDING_DENSITY_VALUE + " INTEGER"
                        + ", " + PlantDensityAbacusItemsColumns.PLANTS_COUNT + " INTEGER"
                        + ")");
                if (newVersion == 3)
                    break;
            }
            case 4:
            {
                database.execSQL("CREATE TABLE IF NOT EXISTS plants ("
                        + PlantsColumns._ID + " INTEGER PRIMARY KEY"
                        + ", " + Plants.NAME + " VARCHAR(255)"
                        + ", " + Plants.SHAPE + " TEXT"
                        + ", " + Plants.VARIETY + " VARCHAR(255)"
                        + ", " + Plants.ACTIVE + " BOOLEAN NOT NULL"
                        + ")");
                if (newVersion == 4)
                    break;
            }
            case 5:
            {
                database.execSQL("ALTER TABLE crumbs ADD user VARCHAR(255)");
                database.execSQL("ALTER TABLE issues ADD user VARCHAR(255)");
                database.execSQL("ALTER TABLE plant_countings ADD user VARCHAR(255)");
                database.execSQL("ALTER TABLE plant_counting_items ADD user VARCHAR(255)");
                if (newVersion == 5)
                    break;
            }
            case 6:
            {
                database.execSQL("ALTER TABLE plant_countings ADD average_value FLOAT");
                database.execSQL("ALTER TABLE plant_countings ADD synced BOOLEAN NOT NULL DEFAULT 0");
                if (newVersion == 6)
                    break;
            }
            case 7:
            {
                /*
                ** Sorry for this part of code,
                ** SQLITE doesn't implement ALTER COLUMN and I had to change ID to be in
                ** autoincrement mode and add the ekylibre instance ID
                ** to deal with multi account problems on IDs.
                */
                database.execSQL("DROP TABLE IF EXISTS plant_density_abaci");
                database.execSQL("DROP TABLE IF EXISTS plant_density_abacus_items");
                database.execSQL("DROP TABLE IF EXISTS plants");
                database.execSQL("CREATE TABLE IF NOT EXISTS plant_density_abaci ("
                        + PlantDensityAbaciColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + PlantDensityAbaciColumns.EK_ID + " INTEGER"
                        + ", " + PlantDensityAbaciColumns.NAME + " VARCHAR(255)"
                        + ", " + PlantDensityAbaciColumns.VARIETY + " VARCHAR(255)"
                        + ", " + PlantDensityAbaciColumns.GERMINATION_PERCENTAGE + " REAL"
                        + ", " + PlantDensityAbaciColumns.SAMPLING_LENGTH_UNIT + " VARCHAR(255)"
                        + ", " + PlantDensityAbaciColumns.SEEDING_DENSITY_UNIT + " VARCHAR(255)"
                        + ", " + PlantDensityAbaciColumns.USER + " VARCHAR(255)"
                        + ")");
                database.execSQL("CREATE TABLE IF NOT EXISTS plant_density_abacus_items ("
                        + PlantDensityAbacusItemsColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + PlantDensityAbacusItemsColumns.EK_ID + " INTEGER"
                        + ", " + PlantDensityAbacusItemsColumns.SEEDING_DENSITY_VALUE + " INTEGER"
                        + ", " + PlantDensityAbacusItemsColumns.PLANTS_COUNT + " INTEGER"
                        + ", " + PlantDensityAbacusItemsColumns.USER + " VARCHAR(255)"
                        + ", " + PlantDensityAbacusItemsColumns.FK_ID + " INTEGER"
                        + ")");
                database.execSQL("CREATE TABLE IF NOT EXISTS plants ("
                        + PlantsColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + Plants.EK_ID + " INTEGER"
                        + ", " + Plants.NAME + " VARCHAR(255)"
                        + ", " + Plants.SHAPE + " TEXT"
                        + ", " + Plants.VARIETY + " VARCHAR(255)"
                        + ", " + Plants.ACTIVE + " BOOLEAN NOT NULL"
                        + ", " + PlantDensityAbacusItemsColumns.USER + " VARCHAR(255)"
                        + ")");
                if (newVersion == 7)
                    break;
            }
            case 8:
            {
                database.execSQL("ALTER TABLE plants ADD activity_ID INTEGER DEFAULT 0");
                database.execSQL("ALTER TABLE plant_density_abaci ADD activity_ID INTEGER DEFAULT 0");
                if (newVersion == 8)
                    break;
            }
            case 9:
            {
                database.execSQL("CREATE TABLE IF NOT EXISTS intervention ("
                        + InterventionsColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + InterventionsColumns.USER + " VARCHAR(255)"
                        + ")");
                database.execSQL("ALTER TABLE crumbs RENAME TO TMP_TABLE");
                database.execSQL("CREATE TABLE IF NOT EXISTS crumbs ("
                        + CrumbsColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + CrumbsColumns.FK_INTERVENTION + " INTEGER"
                        + ", " + CrumbsColumns.TYPE + " VARCHAR(32) NOT NULL"
                        + ", " + CrumbsColumns.LATITUDE + " DOUBLE NOT NULL"
                        + ", " + CrumbsColumns.LONGITUDE + " DOUBLE NOT NULL"
                        + ", " + CrumbsColumns.READ_AT + " BIGINT NOT NULL"
                        + ", " + CrumbsColumns.ACCURACY + " FLOAT"
                        + ", " + CrumbsColumns.SYNCED + " INTEGER NOT NULL DEFAULT 0"
                        + ", " + CrumbsColumns.METADATA + " TEXT"
                        + ", " + CrumbsColumns.USER + " VARCHAR(255)"
                        + ", " + "FOREIGN KEY (" + CrumbsColumns.FK_INTERVENTION + ") REFERENCES intervention(_id)"
                        + ")");
                database.execSQL("INSERT INTO crumbs ("
                        + CrumbsColumns._ID
                        + ", " + CrumbsColumns.TYPE
                        + ", " + CrumbsColumns.LATITUDE
                        + ", " + CrumbsColumns.LONGITUDE
                        + ", " + CrumbsColumns.READ_AT
                        + ", " + CrumbsColumns.ACCURACY
                        + ", " + CrumbsColumns.SYNCED
                        + ", " + CrumbsColumns.METADATA
                        + ", " + CrumbsColumns.USER
                        + ") SELECT "
                        + CrumbsColumns._ID
                        + ", " + CrumbsColumns.TYPE
                        + ", " + CrumbsColumns.LATITUDE
                        + ", " + CrumbsColumns.LONGITUDE
                        + ", " + CrumbsColumns.READ_AT
                        + ", " + CrumbsColumns.ACCURACY
                        + ", " + CrumbsColumns.SYNCED
                        + ", " + CrumbsColumns.METADATA
                        + ", " + CrumbsColumns.USER
                        + " FROM TMP_TABLE");
                database.execSQL("DROP TABLE IF EXISTS TMP_TABLE");
                if (newVersion == 9)
                    break;
            }
            case 10:
            {
                database.execSQL("ALTER TABLE intervention ADD EK_ID INTEGER");
                database.execSQL("ALTER TABLE intervention ADD type VARCHAR(255) DEFAULT NULL");
                database.execSQL("ALTER TABLE intervention ADD procedure_name VARCHAR(255) DEFAULT NULL");
                database.execSQL("ALTER TABLE intervention ADD name VARCHAR(255) DEFAULT NULL");
                database.execSQL("ALTER TABLE intervention ADD number INTEGER DEFAULT 0");
                database.execSQL("ALTER TABLE intervention ADD started_at VARCHAR(255) DEFAULT NULL");
                database.execSQL("ALTER TABLE intervention ADD stopped_at VARCHAR(255) DEFAULT NULL");
                database.execSQL("ALTER TABLE intervention ADD description TEXT DEFAULT NULL");
                database.execSQL("CREATE TABLE IF NOT EXISTS intervention_parameters ("
                        + InterventionParametersColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + InterventionParametersColumns.FK_INTERVENTION + " INTEGER"
                        + ", " + InterventionParametersColumns.EK_ID + " INTEGER"
                        + ", " + InterventionParametersColumns.ROLE + " VARCHAR(255)"
                        + ", " + InterventionParametersColumns.LABEL + " VARCHAR(255)"
                        + ", " + InterventionParametersColumns.NAME + " VARCHAR(255)"
                        + ", " + "FOREIGN KEY (" + CrumbsColumns.FK_INTERVENTION + ") REFERENCES intervention(_id)"
                        + ")");
                if (newVersion == 10)
                    break;
            }
            case 11:
            {
                database.execSQL("ALTER TABLE intervention_parameters ADD product_name VARCHAR(255) DEFAULT NULL");
                database.execSQL("ALTER TABLE intervention_parameters ADD product_id INTEGER DEFAULT 0");
                if (newVersion == 11)
                    break;
            }
            case 12:
            {
                database.execSQL("CREATE TABLE IF NOT EXISTS working_periods ("
                        + WorkingPeriodsColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + WorkingPeriodsColumns.FK_INTERVENTION + " INTEGER"
                        + ", " + WorkingPeriodsColumns.NATURE + " VARCHAR(255)"
                        + ", " + WorkingPeriodsColumns.STARTED_AT + " VARCHAR(255)"
                        + ", " + WorkingPeriodsColumns.STOPPED_AT + " VARCHAR(255)"
                        + ", " + "FOREIGN KEY (" + WorkingPeriodsColumns.FK_INTERVENTION + ") REFERENCES intervention(_id)"
                        + ")");
                if (newVersion == 12)
                    break;
            }
            case 13:
            {
                database.execSQL("ALTER TABLE intervention ADD state VARCHAR(255) DEFAULT NULL");
                database.execSQL("ALTER TABLE intervention ADD request_compliant BOOLEAN " +
                        "DEFAULT 1");
                database.execSQL("ALTER TABLE intervention ADD general_chrono BIGINT " +
                        "DEFAULT NULL");
                database.execSQL("ALTER TABLE intervention ADD preparation_chrono BIGINT " +
                        "DEFAULT NULL");
                database.execSQL("ALTER TABLE intervention ADD travel_chrono BIGINT " +
                        "DEFAULT NULL");
                database.execSQL("ALTER TABLE intervention ADD intervention_chrono BIGINT " +
                        "DEFAULT NULL");
                if (newVersion == 13)
                    break;
            }
            case 14:
            {
                database.execSQL("ALTER TABLE intervention ADD uuid VARCHAR(255)");
                if (newVersion == 14)
                    break;
            }
            case 15:
            {
                database.execSQL("ALTER TABLE plant_countings ADD nature VARCHAR(255)");
                if (newVersion == 15)
                    break;
            }
            case 16:
            {
                database.execSQL("ALTER TABLE intervention_parameters ADD shape text DEFAULT NULL");
                if (newVersion == 16)
                    break;
            }
            case 17:
            {
                database.execSQL("CREATE TABLE IF NOT EXISTS " + Contacts.TABLE_NAME
                        + "("
                        + Contacts._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + Contacts.USER + " VARCHAR(255)"
                        + ", " + Contacts.LAST_NAME + " VARCHAR(255)"
                        + ", " + Contacts.FIRST_NAME + " VARCHAR(255)"
                        + ", " + Contacts.PICTURE + " TEXT"
                        + ", " + Contacts.PICTURE_ID + " INTEGER"
                        + ")");
                database.execSQL("CREATE TABLE IF NOT EXISTS " + ContactParams.TABLE_NAME
                        + "("
                        + ContactParams._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + ContactParams.FK_CONTACT + " INTEGER"
                        + ", " + ContactParams.TYPE + " VARCHAR(255)"
                        + ", " + ContactParams.EMAIL + " VARCHAR(255)"
                        + ", " + ContactParams.PHONE + " VARCHAR(255)"
                        + ", " + ContactParams.MOBILE + " VARCHAR(255)"
                        + ", " + ContactParams.WEBSITE + " VARCHAR(255)"
                        + ", " + ContactParams.MAIL_LINES + " VARCHAR(255)"
                        + ", " + ContactParams.POSTAL_CODE + " VARCHAR(255)"
                        + ", " + ContactParams.CITY + " VARCHAR(255)"
                        + ", " + ContactParams.COUNTRY + " VARCHAR(255)"
                        + ", " + "FOREIGN KEY (" + ContactParams.FK_CONTACT + ") " +
                        "REFERENCES " + Contacts.TABLE_NAME + "(_id)"
                        + ")");
                if (newVersion == 17)
                    break;
            }
            case 18:
            {
                database.execSQL("ALTER TABLE " + Contacts.TABLE_NAME + " ADD " +
                        "organization_name VARCHAR(255)");
                database.execSQL("ALTER TABLE " + Contacts.TABLE_NAME + " ADD " +
                        "organization_post VARCHAR(255)");
                if (newVersion == 18)
                    break;
            }
            case 19:
            {
                database.execSQL("CREATE TABLE IF NOT EXISTS " + LastSyncs.TABLE_NAME
                        + "("
                        + LastSyncs._ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
                        + ", " + LastSyncs.USER + " VARCHAR(255)"
                        + ", " + LastSyncs.TYPE + " VARCHAR(255)"
                        + ", " + LastSyncs.DATE + " VARCHAR(255)"
                        + ")");
                if (newVersion == 19)
                    break;
            }
            case 20:
            {
                database.execSQL("ALTER TABLE " + Contacts.TABLE_NAME + " ADD " +
                        "type VARCHAR(255)");
                database.execSQL("ALTER TABLE " + Contacts.TABLE_NAME + " ADD " +
                        "ek_id INTEGER");
                if (newVersion == 20)
                    break;
            }

            // New observation feature
            case 21:
            {

//                database.execSQL("ALTER TABLE " + Issues.TABLE_NAME + " ADD " +
//                        Issues.EKY_ID + " INTEGER DEFAULT NULL");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + IssueNatures.TABLE_NAME + "(" +
                        IssueNatures._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        IssueNatures.CATEGORY + " TEXT NOT NULL, " +
                        IssueNatures.LABEL + " TEXT NOT NULL, " +
                        IssueNatures.NATURE + " TEXT NOT NULL)");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + VegetalScale.TABLE_NAME + "(" +
                        VegetalScale._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        VegetalScale.REFERENCE + " INTEGER NOT NULL, " +
                        VegetalScale.LABEL + " TEXT NOT NULL, " +
                        VegetalScale.VARIETY + " TEXT NOT NULL," +
                        VegetalScale.POSITION + " INTEGER NOT NULL)");

                database.execSQL("ALTER TABLE " + Plants.TABLE_NAME + " ADD " +
                        Plants.ACTIVITY_NAME + " TEXT DEFAULT NULL");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + Observations.TABLE_NAME + "(" +
                        Observations._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        Observations.OBSERVED_ON + " DATETIME NOT NULL, " +
                        Observations.ACTIVITY_ID + " INTEGER NOT NULL, " +
                        Observations.PLANTS + " TEXT, " +
                        Observations.SCALE_ID + " INTEGER, " +
                        Observations.DESCRIPTION + " TEXT, " +
                        Observations.PICTURES + " TEXT, " +
                        Observations.LONGITUDE + " DOUBLE, " +
                        Observations.LATITUDE + " DOUBLE, " +
                        Observations.SYNCED + " BOOLEAN NOT NULL DEFAULT 0, " +
                        Observations.USER + " TEXT)");

//                database.execSQL("CREATE TABLE IF NOT EXISTS " + ObservationPlants.TABLE_NAME + "(" +
//                        ObservationPlants._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
//                        ObservationPlants.FK_OBSERVATION + " INTEGER NOT NULL, " +
//                        ObservationPlants.FK_PLANT + " INTEGER NOT NULL, " +
//                        "FOREIGN KEY (" + ObservationPlants.FK_OBSERVATION + ") " +
//                            "REFERENCES " + Observations.TABLE_NAME + "(_id), " +
//                            "FOREIGN KEY (" + ObservationPlants.FK_PLANT + ") " +
//                        "REFERENCES " + Plants.TABLE_NAME + "(_id))");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + ObservationIssues.TABLE_NAME + "(" +
                        ObservationIssues._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        ObservationIssues.FK_OBSERVATION + " INTEGER NOT NULL, " +
                        ObservationIssues.FK_ISSUE + " INTEGER NOT NULL, " +
                        ObservationIssues.EKY_ID_ISSUE + " INTEGER, " +
                        "FOREIGN KEY (" + ObservationIssues.FK_OBSERVATION + ") " +
                            "REFERENCES " + Observations.TABLE_NAME + "(_id), " +
                        "FOREIGN KEY (" + ObservationIssues.FK_ISSUE + ") " +
                            "REFERENCES " + Issues.TABLE_NAME + "(_id))");

                if (newVersion == 21)
                    break;
            }
            case 22:
            {
//
//                database.execSQL("ALTER TABLE " + Plants.TABLE_NAME
//                        + " ADD " + Plants.NET_SURFACE_AREA + " TEXT DEFAULT NULL");
//                database.execSQL("ALTER TABLE " + Plants.TABLE_NAME
//                        + " ADD " + Plants.DEAD_AT + " DATETIME DEFAULT NULL");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + DetailedInterventions.TABLE_NAME + "(" +
                        DetailedInterventions._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DetailedInterventions.PROCEDURE_NAME + " TEXT NOT NULL, " +
                        DetailedInterventions.CREATED_ON + " DATETIME NOT NULL, " +
                        DetailedInterventions.EK_ID + " INTEGER, " +
                        DetailedInterventions.USER + " TEXT)");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + DetailedInterventionAttributes.TABLE_NAME + "(" +
                        DetailedInterventionAttributes._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DetailedInterventionAttributes.DETAILED_INTERVENTION_ID + " INTEGER NOT NULL, " +
                        DetailedInterventionAttributes.ROLE + " TEXT NOT NULL, " +
                        DetailedInterventionAttributes.REFERENCE_NAME + " TEXT NOT NULL, " +
                        DetailedInterventionAttributes.REFERENCE_ID + " INTEGER NOT NULL, " +
                        DetailedInterventionAttributes.QUANTITY_VALUE + " REAL, " +
                        "quantity_unit_name TEXT, " +
                        "FOREIGN KEY ("+DetailedInterventionAttributes.DETAILED_INTERVENTION_ID +") " +
                        "REFERENCES "+DetailedInterventions.TABLE_NAME+"("+DetailedInterventions._ID+"));");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + WorkingPeriodAttributes.TABLE_NAME + "(" +
                        WorkingPeriodAttributes._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        WorkingPeriodAttributes.DETAILED_INTERVENTION_ID + " INTEGER NOT NULL, " +
                        WorkingPeriodAttributes.STARTED_AT + " DATETIME NOT NULL, " +
                        WorkingPeriodAttributes.STOPPED_AT + " DATETIME NOT NULL, " +
                        "FOREIGN KEY ("+WorkingPeriodAttributes.DETAILED_INTERVENTION_ID +") " +
                        "REFERENCES "+DetailedInterventions.TABLE_NAME+"("+DetailedInterventions._ID+"));");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + Equipments.TABLE_NAME + "(" +
                        Equipments.EK_ID + " INTEGER PRIMARY KEY, " +
                        Equipments.NAME + " TEXT NOT NULL, " +
                        Equipments.NUMBER + " TEXT, " +
                        Workers.WORK_NUMBER + " TEXT, " +
                        Equipments.VARIETY + " TEXT NOT NULL, " +
                        Equipments.ABILITIES + " TEXT, " +
                        Equipments.DEAD_AT + " DATETIME, " +
                        Equipments.USER + " TEXT)");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + Workers.TABLE_NAME + "(" +
                        Workers.EK_ID + " INTEGER PRIMARY KEY, " +
                        Workers.NAME + " TEXT NOT NULL, " +
                        Workers.NUMBER + " TEXT, " +
                        Workers.WORK_NUMBER + " TEXT, " +
                        Workers.VARIETY + " TEXT NOT NULL, " +
                        Workers.ABILITIES + " TEXT, " +
                        Workers.DEAD_AT + " DATETIME, " +
                        Workers.USER + " TEXT)");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + LandParcels.TABLE_NAME + "(" +
                        LandParcels.EK_ID + " INTEGER PRIMARY KEY, " +
                        LandParcels.NAME + " TEXT NOT NULL, " +
                        LandParcels.NET_SURFACE_AREA + " TEXT, " +
                        LandParcels.DEAD_AT + " DATETIME, " +
                        LandParcels.USER + " TEXT)");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + BuildingDivisions.TABLE_NAME + "(" +
                        LandParcels.EK_ID + " INTEGER PRIMARY KEY, " +
                        LandParcels.NAME + " TEXT NOT NULL, " +
                        LandParcels.NET_SURFACE_AREA + " TEXT, " +
                        LandParcels.DEAD_AT + " DATETIME, " +
                        LandParcels.USER + " TEXT)");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + Inputs.TABLE_NAME + "(" +
                        Inputs.EK_ID + " INTEGER PRIMARY KEY, " +
                        Inputs.NAME + " TEXT NOT NULL, " +
                        Inputs.VARIETY + " TEXT, " +
                        Inputs.ABILITIES + " TEXT, " +
                        Inputs.NUMBER + " TEXT, " +
                        Inputs.POPULATION + " REAL, " +
                        Inputs.CONTAINER_NAME + " TEXT, " +
                        Inputs.USER + " TEXT)");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + Outputs.TABLE_NAME + "(" +
                        Outputs.EK_ID + " INTEGER PRIMARY KEY, " +
                        Outputs.NAME + " TEXT NOT NULL, " +
                        Outputs.VARIETY + " TEXT, " +
                        Outputs.NUMBER + " TEXT, " +
                        Outputs.ABILITIES + " TEXT, " +
                        Outputs.USER + " TEXT)");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + Products.TABLE_NAME + "(" +
                        Products.EK_ID + " INTEGER PRIMARY KEY, " +
                        Products.NAME + " TEXT NOT NULL, " +
                        Products.NUMBER + " TEXT, " +
                        Products.WORK_NUMBER + " TEXT, " +
                        Products.VARIETY + " TEXT, " +
                        Products.ABILITIES + " TEXT, " +
                        Products.POPULATION + " TEXT, " +
                        Products.UNIT + " TEXT, " +
                        Products.CONTAINER_NAME + " TEXT, " +
                        Products.DEAD_AT + " DATETIME, " +
                        Products.NET_SURFACE_AREA + " TEXT, " +
                        Products.USER + " TEXT)");

                if (newVersion == 22)
                    break;
            }

            case 23:
            {
                database.execSQL("ALTER TABLE " + DetailedInterventionAttributes.TABLE_NAME
                        + " ADD " + DetailedInterventionAttributes.GROUP_ID + " INTEGER DEFAULT NULL");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + GroupZones.TABLE_NAME + "(" +
                        GroupZones._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        GroupZones.DETAILED_INTERVENTION_ID + " INTEGER NOT NULL, " +
                        GroupZones.TARGET_ID + " INTEGER, " +
                        GroupZones.OUTPUT_ID + " INTEGER, " +
                        GroupZones.NEW_NAME + " TEXT, " +
                        GroupZones.BATCH_NUMBER + " TEXT, " +
                        "FOREIGN KEY ("+GroupZones.DETAILED_INTERVENTION_ID +") " +
                        "REFERENCES " + DetailedInterventions.TABLE_NAME + "(" + DetailedInterventions._ID + "))");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + Variants.TABLE_NAME + "(" +
                        Variants.EK_ID + " INTEGER PRIMARY KEY, " +
                        Variants.NAME + " INTEGER NOT NULL, " +
                        Variants.NUMBER + " TEXT, " +
                        Variants.VARIETY + " INTEGER NOT NULL, " +
                        Variants.ABILITIES + " TEXT ," +
                        Variants.USER + " TEXT)");

                database.execSQL("ALTER TABLE " + Products.TABLE_NAME
                        + " ADD " + Products.PRODUCTION_STARTED_ON + " DATETIME");

                database.execSQL("ALTER TABLE " + Products.TABLE_NAME
                        + " ADD " + Products.PRODUCTION_STOPPED_ON + " DATETIME");

                if (newVersion == 23)
                    break;
            }

            case 24:

                for (Account account : AccountTool.getListAccount(context))
                    prefs.edit().remove("last_sync_date_" + account.name).apply();

                if (newVersion == 24)
                    break;

            case 25:

                database.execSQL("ALTER TABLE " + DetailedInterventions.TABLE_NAME
                        + " ADD " + DetailedInterventions.ACTIONS + " TEXT");

                database.execSQL("CREATE TABLE IF NOT EXISTS " + GroupWorks.TABLE_NAME + "(" +
                        GroupWorks._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        GroupWorks.DETAILED_INTERVENTION_ID + " INTEGER NOT NULL, " +
                        GroupWorks.TARGET_ID + " INTEGER, " +
                        GroupWorks.INPUTS_ID + " TEXT, " +
                        "FOREIGN KEY ("+GroupZones.DETAILED_INTERVENTION_ID +") " +
                        "REFERENCES " + DetailedInterventions.TABLE_NAME + "(" + DetailedInterventions._ID + "))");

                if (newVersion == 25)
                    break;

            case 26:

                database.execSQL("ALTER TABLE " + Interventions.TABLE_NAME
                        + " ADD " + Interventions.DETAILED_INTERVENTION_ID + " INTEGER");

                database.execSQL("ALTER TABLE " + DetailedInterventions.TABLE_NAME
                        + " ADD " + DetailedInterventions.STATUS + " TEXT");

                database.execSQL("ALTER TABLE " + DetailedInterventionAttributes.TABLE_NAME
                        + " ADD " + DetailedInterventionAttributes.NAME + " TEXT");

                database.execSQL("ALTER TABLE " + DetailedInterventionAttributes.TABLE_NAME
                        + " ADD unit_name TEXT");

                database.execSQL("ALTER TABLE " + WorkingPeriodAttributes.TABLE_NAME
                        + " ADD " + WorkingPeriodAttributes.NATURE + " TEXT");

//                // Copy working_period_attributes table rows to working_periods table and then drop it
//                database.execSQL("INSERT INTO " + WorkingPeriods.TABLE_NAME +
//                        "(" + WorkingPeriods.FK_INTERVENTION + ", " + WorkingPeriods.STARTED_AT + ", " + WorkingPeriods.STOPPED_AT + ") " +
//                        "SELECT " + WorkingPeriodAttributes.DETAILED_INTERVENTION_ID + ", " +
//                        WorkingPeriodAttributes.STARTED_AT + ", " + WorkingPeriodAttributes.STOPPED_AT +
//                        " FROM " + WorkingPeriodAttributes.TABLE_NAME);
//
//                database.execSQL("DROP TABLE IF EXISTS " + WorkingPeriodAttributes.TABLE_NAME);

                if (newVersion == 26)
                    break;

            case 27:

                database.execSQL("ALTER TABLE " + DetailedInterventionAttributes.TABLE_NAME
                        + " ADD " + DetailedInterventionAttributes.HOUR_METER + " FLOAT");

                if (newVersion == 27)
                    break;

            case 28:

                database.execSQL("CREATE INDEX products_user_idx ON products(user)");
                database.execSQL("CREATE INDEX products_production_stopped_on_idx ON products(production_stopped_on)");
                database.execSQL("CREATE INDEX products_work_number_idx ON products(work_number)");
                database.execSQL("CREATE INDEX products_abilities_idx ON products(abilities)");

                database.execSQL("CREATE INDEX variants_user_idx ON variants(user)");
                database.execSQL("CREATE INDEX variants_number_idx ON variants(number)");
                database.execSQL("CREATE INDEX variants_abilities_idx ON variants(abilities)");

                if (newVersion == 28)
                    break;

            case 29:

                database.execSQL("ALTER TABLE products ADD search_name TEXT");
                database.execSQL("ALTER TABLE variants ADD search_name TEXT");

                database.execSQL("CREATE INDEX products_search_name_idx ON products(search_name)");
                database.execSQL("CREATE INDEX variants_search_name_idx ON variants(search_name)");

                if (newVersion == 29)
                    break;

            case 30:

                database.execSQL("ALTER TABLE intervention_parameters ADD hour_meter REAL");

                if (newVersion == 30)
                    break;

//                database.execSQL("CREATE VIRTUAL TABLE products_search " +
//                        "USING FTS4(ek_id, name, number, work_number, variety, abilities, " +
//                        "notindexed=ek_id)");
//                database.execSQL("INSERT INTO products_search SELECT ek_id, name, number, work_number FROM products");
//
//                try (Cursor cursor = database.rawQuery(
//                        "SELECT id, name, work_number FROM products_search " +
//                                "WHERE products_search MATCH 'filt*'", null)) {
//                    while (cursor != null && cursor.moveToNext()) {
//                        Log.i("DatabaseHelper", "Result -> " + cursor.getString(0) + " | " + cursor.getString(1));
//                    }
//                }

            case 31:

                // Remove old unused tables

                database.execSQL("DROP TABLE IF EXISTS " + Equipments.TABLE_NAME);
                database.execSQL("DROP TABLE IF EXISTS " + Inputs.TABLE_NAME);
                database.execSQL("DROP TABLE IF EXISTS " + Outputs.TABLE_NAME);
                database.execSQL("DROP TABLE IF EXISTS " + Workers.TABLE_NAME);
                database.execSQL("DROP TABLE IF EXISTS " + LandParcels.TABLE_NAME);
                database.execSQL("DROP TABLE IF EXISTS " + BuildingDivisions.TABLE_NAME);

                if (newVersion == 31)
                    break;

            case 32:

                // Rebuild products table with composite primary key

                database.execSQL("DROP INDEX IF EXISTS products_user_idx");
                database.execSQL("DROP INDEX IF EXISTS products_search_name_idx");
                database.execSQL("DROP INDEX IF EXISTS products_production_stopped_on_idx");
                database.execSQL("DROP INDEX IF EXISTS products_work_number_idx");
                database.execSQL("DROP INDEX IF EXISTS products_abilities_idx");
                database.execSQL("DROP TABLE IF EXISTS products");

                database.execSQL("CREATE TABLE IF NOT EXISTS products (" +
                        Products.EK_ID + " INTEGER, " +
                        Products.NAME + " TEXT NOT NULL, " +
                        Products.SEARCH_NAME + " TEXT, " +
                        Products.NUMBER + " TEXT, " +
                        Products.WORK_NUMBER + " TEXT, " +
                        Products.VARIETY + " TEXT, " +
                        Products.ABILITIES + " TEXT, " +
                        Products.POPULATION + " TEXT, " +
                        Products.UNIT + " TEXT, " +
                        Products.CONTAINER_NAME + " TEXT, " +
                        Products.DEAD_AT + " DATETIME, " +
                        Products.PRODUCTION_STARTED_ON + " DATETIME, " +
                        Products.PRODUCTION_STOPPED_ON + " DATETIME, " +
                        Products.NET_SURFACE_AREA + " TEXT, " +
                        Products.USER + " TEXT, " +
                        "PRIMARY KEY (" + Products.EK_ID + ", " + Products.USER + "))");

                database.execSQL("CREATE INDEX products_idx ON products(ek_id, user)");
                database.execSQL("CREATE INDEX products_production_stopped_on_idx ON products(production_stopped_on)");
                database.execSQL("CREATE INDEX products_work_number_idx ON products(work_number)");
                database.execSQL("CREATE INDEX products_abilities_idx ON products(abilities)");


                // Rebuild variants

                database.execSQL("DROP INDEX IF EXISTS variants_user_idx");
                database.execSQL("DROP INDEX IF EXISTS variants_search_name_idx");
                database.execSQL("DROP INDEX IF EXISTS variants_abilities_idx");
                database.execSQL("DROP TABLE IF EXISTS variants");

                database.execSQL("CREATE TABLE IF NOT EXISTS variants (" +
                        Variants.EK_ID + " INTEGER, " +
                        Variants.NAME + " TEXT NOT NULL, " +
                        Variants.SEARCH_NAME + " TEXT, " +
                        Variants.NUMBER + " TEXT, " +
                        Variants.VARIETY + " TEXT, " +
                        Variants.ABILITIES + " TEXT, " +
                        Variants.USER + " TEXT, " +
                        "PRIMARY KEY (" + Variants.EK_ID + ", " + Variants.USER + "))");

                database.execSQL("CREATE INDEX variants_idx ON variants(ek_id, user)");
                database.execSQL("CREATE INDEX variants_abilities_idx ON variants(abilities)");


                // Reset last sync date to force full sync

                for (Account account : AccountTool.getListAccount(context))
                    prefs.edit().remove("last_sync_date_" + account.name).apply();

                if (newVersion == 32)
                    break;

            case 33:
//                MainActivity.accountChanged = true;

                if (newVersion == 33)
                    break;

            case 34:
                database.execSQL("ALTER TABLE " + DetailedInterventions.TABLE_NAME +
                        " ADD description TEXT");
                database.execSQL("ALTER TABLE " + Products.TABLE_NAME +
                        " ADD has_hour_counter BOOLEAN");

                for (Account account : AccountTool.getListAccount(context))
                    prefs.edit().remove("last_sync_date_" + account.name).apply();

                if (newVersion == 34)
                    break;

            case 35:
                // DEPRECATED MIGRATION
                // SyncAdapter.onlyVariants = true;
                if (newVersion == 35)
                    break;

            case 36:

//                database.execSQL("ALTER TABLE plants RENAME TO plants_tmp");
                database.execSQL("DROP TABLE IF EXISTS plants");

                database.execSQL("CREATE TABLE IF NOT EXISTS plants ("
                        + Plants.EK_ID + " INTEGER, "
                        + Plants.NAME + " TEXT, "
                        + Plants.SHAPE + " TEXT, "
                        + Plants.VARIETY + " TEXT, "
                        + Plants.ACTIVE + " BOOLEAN NOT NULL, "
                        + Plants.ACTIVITY_NAME + " TEXT DEFAULT NULL, "
                        + Plants.ACTIVITY_ID + " INTEGER DEFAULT 0, "
                        + Plants.USER + " TEXT, " +
                        "PRIMARY KEY (" + Plants.EK_ID + ", " + Plants.USER + "))");

                if (newVersion == 36)
                    break;

            case 37:

                // TODO :: to force a full sync, reset the lastSyncDate attribute
                for (Account account : AccountTool.getListAccount(context))
                    prefs.edit().remove("last_sync_date_" + account.name).apply();

                if (newVersion == 37)
                    break;

            case 38:

                // Set old
                database.execSQL("UPDATE intervention SET state = 'SYNCED'");

                if (newVersion == 38)
                    break;

            case 39:
                Log.e("SQL", "Run Search Name migration");
                LongMigrationService.startActionSearchNames(context);

                if (newVersion == 39)
                    break;

            case 40:
                database.execSQL("ALTER TABLE products ADD born_at DATETIME");
                prefs.edit().putBoolean("force_sync_plant", true).apply();

                if (newVersion == 40)
                    break;

            case 41:
                for (String type : productTypes)
                    editor.putBoolean("force_sync_" + type, true);
                editor.apply();

                if (newVersion == 41)
                    break;

            case 42:
                database.execSQL("UPDATE working_period_attributes " +
                        "SET started_at = strftime('%Y-%m-%dT%H:%M:%S+0100', started_at/1000, 'unixepoch', 'localtime') " +
                        "WHERE started_at NOT LIKE '%T%'");
                database.execSQL("UPDATE working_period_attributes " +
                        "SET stopped_at = strftime('%Y-%m-%dT%H:%M:%S+0100', stopped_at/1000, 'unixepoch', 'localtime') " +
                        "WHERE stopped_at NOT LIKE '%T%'");
                if (newVersion == 42)
                    break;
            case 43:
                if (newVersion == 43)
                    break;
            case 44:
                if (newVersion == 44)
                    break;
            case 45:
                // Set hour counter for "Pulvérisateurs"
                database.execSQL("UPDATE products SET has_hour_counter = 1 WHERE work_number LIKE 'APP%'");
                Log.e("MIGRATIONS", "Pulvérisateurs");
                if (newVersion == 45)
                    break;

            case 46:

                // Rename the old table
                database.execSQL("ALTER TABLE detailed_intervention_attributes RENAME TO old");

                // Create new table according to new desired schema
                database.execSQL("CREATE TABLE IF NOT EXISTS detailed_intervention_attributes (" +
                        "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "detailed_intervention_id INTEGER NOT NULL, " +
                        "name TEXT, " +
                        "role TEXT NOT NULL, " +
                        "reference_id TEXT NOT NULL, " +
                        "reference_name INTEGER NOT NULL, " +
                        "quantity_value REAL, " +
                        "quantity_unit TEXT, " +
                        "quantity_indicator TEXT, " +
                        "quantity_name TEXT, " +
                        "unit_label TEXT, " +
                        "group_id INTEGER, " +
                        "hour_meter REAL, " +
                        "FOREIGN KEY (detailed_intervention_id) REFERENCES detailed_intervention(_id));");

                // Copy data into new table
                database.execSQL("INSERT INTO detailed_intervention_attributes SELECT _id, " +
                        "detailed_intervention_id, name, role, reference_id, reference_name, " +
                        "quantity_value, NULL, quantity_unit_name, NULL, unit_name, group_id, hour_meter " +
                        "FROM old");

                // Drop old table
                database.execSQL("DROP TABLE IF EXISTS old");

                // Run data migration on already rows
//                LongMigrationService.startActionMigrateUnits(context);

                if (newVersion == 46)
                    break;

            case 47:
                // Migration to disable failed interventions from sync retry
                database.execSQL("UPDATE detailed_interventions SET ek_id = -2 WHERE ek_id = -1");
                Log.e("MIGRATIONS", "Suppression interventions avec erreurs");
                // other solution, delete ->
                // database.execSQL("DELETE FROM detailed_interventions WHERE ek_id = -1");
                if (newVersion == 47)
                    break;
            case 48:
                if (newVersion == 48)
                    break;
            case 49:
                if (newVersion == 49)
                    break;
            case 50:
                // Migration to delete failed interventions from sync retry
                database.execSQL("DELETE FROM intervention WHERE " + InterventionsColumns.STATE + " = \"FINISHED\"");
                Log.e("MIGRATIONS", "Suppression interventions sync");
                // other solution, delete ->
                // database.execSQL("DELETE FROM detailed_interventions WHERE ek_id = -1");
                if (newVersion == 50)
                    break;

            case 51:
                List<Integer> ids = Arrays.asList(1852, 20443, 20442, 20441, 1849, 20440, 20439, 1848, 1850, 1851, 20446, 20445, 20444);
                for (Integer id: ids) {
                    database.execSQL("UPDATE products SET has_hour_counter = 1 WHERE " + Products.EK_ID + " LIKE " + id);
                }
                Log.e("MIGRATIONS", "Moisonneuses");

                if (newVersion == 51)
                    break;
            case 52:
                if (newVersion == 52)
                    break;
            case 53:
                database.execSQL("CREATE TABLE IF NOT EXISTS phytosanitary_references ("
                        + PhytosanitaryReferences.ID + " INTEGER PRIMARY KEY, "
                        + PhytosanitaryReferences.PRODUCT_TYPE + " TEXT, "
                        + PhytosanitaryReferences.REFERENCE_NAME + " TEXT NOT NULL, "
                        + PhytosanitaryReferences.NAME + " TEXT NOT NULL, "
                        + PhytosanitaryReferences.OTHER_NAME + " TEXT DEFAULT NULL, "
                        + PhytosanitaryReferences.NATURE + " TEXT DEFAULT NULL, "
                        + PhytosanitaryReferences.ACTIVE_COMPOUNDS + " TEXT DEFAULT NULL, "
                        + PhytosanitaryReferences.MIX_CODE + " INTEGER DEFAULT 0, "
                        + PhytosanitaryReferences.FIELD_REENTRY_DELAY + " INTEGER, "
                        + PhytosanitaryReferences.STATE + " TEXT, "
                        + PhytosanitaryReferences.STARTED_ON + " DATE DEFAULT NULL, "
                        + PhytosanitaryReferences.STOPPED_ON + " DATE DEFAULT NULL, "
                        + PhytosanitaryReferences.ALLOWED_MENTIONS + " TEXT DEFAULT NULL, "
                        + PhytosanitaryReferences.RESTRICTED_MENTIONS + " TEXT DEFAULT NULL, "
                        + PhytosanitaryReferences.OPERATOR_PROTECTION + " TEXT DEFAULT NULL, "
                        + PhytosanitaryReferences.FIRM_NAME + " TEXT, "
                        + PhytosanitaryReferences.RECORD_CHECKSUM + " TEXT"
                        + ")"
                );

                database.execSQL("CREATE TABLE IF NOT EXISTS phytosanitary_usages ("
                        + PhytosanitaryUsages.ID + " TEXT PRIMARY KEY, "
                        + PhytosanitaryUsages.PRODUCT_ID + " INTEGER NOT NULL, "
                        + PhytosanitaryUsages.EPHY_USAGE_PHRASE + " TEXT, "
                        + PhytosanitaryUsages.CROP + " TEXT, "
                        + PhytosanitaryUsages.SPECIES + " TEXT, "
                        + PhytosanitaryUsages.TARGET_NAME + " TEXT, "
                        + PhytosanitaryUsages.DESCRIPTION + " TEXT DEFAULT NULL, "
                        + PhytosanitaryUsages.TREATMENT + " TEXT DEFAULT NULL, "
                        + PhytosanitaryUsages.DOSE_QUANTITY + " FLOAT DEFAULT NULL, "
                        + PhytosanitaryUsages.DOSE_UNIT + " TEXT DEFAULT NULL, "
                        + PhytosanitaryUsages.DOSE_UNIT_NAME + " TEXT DEFAULT NULL, "
                        + PhytosanitaryUsages.DOSE_UNIT_FACTOR + " FLOAT DEFAULT NULL, "
                        + PhytosanitaryUsages.HARVEST_DELAY_DAYS + " DATE DEFAULT NULL, "
                        + PhytosanitaryUsages.HARVEST_DELAY_BBCH + " TEXT DEFAULT NULL, "
                        + PhytosanitaryUsages.APPLICATIONS_COUNT + " INTEGER DEFAULT NULL, "
                        + PhytosanitaryUsages.APPLICATIONS_FREQUENCY + " INTEGER DEFAULT NULL, "
                        + PhytosanitaryUsages.DEVELOPMENT_STAGE_MIN + " INTEGER DEFAULT NULL, "
                        + PhytosanitaryUsages.DEVELOPMENT_STAGE_MAX + " INTEGER DEFAULT NULL, "
                        + PhytosanitaryUsages.USAGE_CONDITIONS + " TEXT DEFAULT NULL, "
                        + PhytosanitaryUsages.AQUATIC_BUFFER + " INTEGER DEFAULT NULL, "
                        + PhytosanitaryUsages.ARTHROPOD_BUFFER + " INTEGER DEFAULT NULL, "
                        + PhytosanitaryUsages.PLANT_BUFFER + " INTEGER DEFAULT NULL, "
                        + PhytosanitaryUsages.DECISION_DATE + " TEXT, "
                        + PhytosanitaryUsages.STATE + " TEXT, "
                        + PhytosanitaryUsages.RECORD_CHECKSUM + " TEXT"
                        + ")"
                );

                database.execSQL("CREATE INDEX IF NOT EXISTS phytosanitary_references_idx ON phytosanitary_references(id)");
                database.execSQL("CREATE INDEX IF NOT EXISTS phytosanitary_usages_idx ON phytosanitary_usages(id)");
                database.execSQL("CREATE INDEX IF NOT EXISTS phytosanitary_usages_product_idx ON phytosanitary_usages(product_id)");

                database.execSQL("CREATE TABLE IF NOT EXISTS ephy_cropsets ("
                        + EphyCropsets.ID + " INTEGER PRIMARY KEY, "
                        + EphyCropsets.NAME + " TEXT, "
                        + EphyCropsets.LABEL + " TEXT, "
                        + EphyCropsets.CROP_NAMES + " TEXT, "
                        + EphyCropsets.CROP_LABELS + " TEXT, "
                        + EphyCropsets.RECORD_CHECKSUM + " TEXT"
                        + ")"
                );

                database.execSQL("CREATE INDEX IF NOT EXISTS ephy_cropsets_name_idx ON ephy_cropsets(name)");

                database.execSQL("ALTER TABLE " + Products.TABLE_NAME +
                        " ADD ref_id TEXT DEFAULT NULL");

                database.execSQL("ALTER TABLE " + DetailedInterventionAttributes.TABLE_NAME +
                        " ADD phyto_usage TEXT DEFAULT NULL");

                editor.putBoolean("force_sync_matter", true);
                editor.apply();

                if (newVersion == 53)
                    break;
        }
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) { e.printStackTrace();}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
}