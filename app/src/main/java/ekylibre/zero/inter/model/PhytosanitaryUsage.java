package ekylibre.zero.inter.model;

import android.database.Cursor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ekylibre.database.ZeroContract.PhytosanitaryUsages;

public class PhytosanitaryUsage {

    public String id;
    public int productId;
    public String ephyUsagePhrase;
    public String crop;
    public String species;
    public String targetName;
    public String description;
    public String treatment;
    public Float doseQuantity;
    public String doseUnit;
    public String doseUnitName;
    public Float doseUnitFactor;
    public Integer harvestDelayDays;
    public String harvestDelayBBCH;
    public Integer applicationsCount;
    public String applicationsFrequency;
    public String developmentStageMin;
    public String developmentStageMax;
    public String usageConditions;
    public Integer aquaticBuffer;
    public Integer arthropodBuffer;
    public Integer plantBuffer;
    public String decisionDate;
    public String state;
    public String ephyCropsets;
    public String doseUnitDimension;

    private PhytosanitaryUsage() { }

    public static PhytosanitaryUsage getPhytosanitaryUsage(Cursor curs) {

        final int aquaticBufferIndex = curs.getColumnIndex(PhytosanitaryUsages.AQUATIC_BUFFER);
        final int arthropodBufferIndex = curs.getColumnIndex(PhytosanitaryUsages.ARTHROPOD_BUFFER);
        final int plantBufferIndex = curs.getColumnIndex(PhytosanitaryUsages.PLANT_BUFFER);
        final int doseQuantityIndex = curs.getColumnIndex(PhytosanitaryUsages.DOSE_QUANTITY);
        final int applicationsCountIndex = curs.getColumnIndex(PhytosanitaryUsages.APPLICATIONS_COUNT);

        PhytosanitaryUsage usage =  new PhytosanitaryUsage();

        usage.id = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.ID));
        usage.productId = curs.getInt(curs.getColumnIndex(PhytosanitaryUsages.PRODUCT_ID));
        usage.ephyUsagePhrase = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.EPHY_USAGE_PHRASE));
        usage.crop = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.CROP));
        usage.species = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.SPECIES));
        usage.targetName = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.TARGET_NAME));
        usage.description = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.DESCRIPTION));
        usage.treatment = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.TREATMENT));
        usage.doseQuantity = curs.isNull(doseQuantityIndex) ? null : curs.getFloat(doseQuantityIndex);
        usage.doseUnit = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.DOSE_UNIT));
        usage.doseUnitName = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.DOSE_UNIT_NAME));
        usage.doseUnitFactor = curs.getFloat(curs.getColumnIndex(PhytosanitaryUsages.DOSE_UNIT_FACTOR));
        usage.harvestDelayDays = curs.getInt(curs.getColumnIndex(PhytosanitaryUsages.HARVEST_DELAY_DAYS));
        usage.harvestDelayBBCH = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.HARVEST_DELAY_BBCH));
        usage.applicationsCount = curs.isNull(applicationsCountIndex) ? null : curs.getInt(applicationsCountIndex);
        usage.applicationsFrequency = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.APPLICATIONS_FREQUENCY));
        usage.developmentStageMin = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.DEVELOPMENT_STAGE_MIN));
        usage.developmentStageMax = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.DEVELOPMENT_STAGE_MAX));
        usage.usageConditions = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.USAGE_CONDITIONS));
        usage.aquaticBuffer = curs.isNull(aquaticBufferIndex) ? null : curs.getInt(aquaticBufferIndex);
        usage.arthropodBuffer = curs.isNull(arthropodBufferIndex) ? null : curs.getInt(arthropodBufferIndex);
        usage.plantBuffer = curs.isNull(plantBufferIndex) ? null : curs.getInt(plantBufferIndex);
        usage.decisionDate = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.DECISION_DATE));
        usage.state = curs.getString(curs.getColumnIndex(PhytosanitaryUsages.STATE));
        usage.ephyCropsets = null;
        usage.doseUnitDimension = null;

        return usage;
    }

    //to display object as a string in spinner
    @NonNull
    @Override
    public String toString() {
//        String.format("%s * %s", crop, targetName)
        return ephyUsagePhrase;
    }

    public static PhytosanitaryUsage getEmptyDefaultUsage() {
        PhytosanitaryUsage defaultUsage = new PhytosanitaryUsage();
        defaultUsage.id = "-1";
        defaultUsage.ephyUsagePhrase = " - - -";
        return defaultUsage;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof PhytosanitaryUsage) {
            final PhytosanitaryUsage usage = (PhytosanitaryUsage) obj;
            return usage.id.equals(this.id);
        } else
            return false;
    }
}
