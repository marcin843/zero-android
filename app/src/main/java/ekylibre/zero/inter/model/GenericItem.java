package ekylibre.zero.inter.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import ekylibre.util.Unit;

import ekylibre.util.pojo.SpinnerItem;

public class GenericItem {

    public int id;
    public String name;
    public String number;
    public String workNumber;
    public String variety;
    public String[] abilities;
    public BigDecimal quantity;
    public SpinnerItem unit_spinner;
    public String unit;
    public String unitString;
    public HashMap<String, String> referenceName;
    public String population;
    public String containerName;
    public String netSurfaceArea;
    public Date born_at;
    public Date dead_at;
//    public Date production_started_at;
//    public Date production_stopped_at;
    public BigDecimal hourMeter;
    public boolean hasHourMeter;
    public Integer refId;
    public PhytosanitaryUsage phytoUsage;
    public Unit phytoUnit;

    public GenericItem() {
        this.quantity = null;
        this.referenceName = new HashMap<>();
    }

    @Override
    public boolean equals(Object o) {

        if (o instanceof GenericItem)
            return this.id == ((GenericItem) o).id;

        return false;
    }
}
