package ekylibre.zero.inter.model;

import java.util.ArrayList;
import java.util.List;

public class Work {

    public GenericItem equipment;
    public List<GenericItem> replacementParts;
    public List<GenericItem> consumableParts;

    public Work() {
        this.replacementParts = new ArrayList<>();
        this.consumableParts = new ArrayList<>();
    }
}
