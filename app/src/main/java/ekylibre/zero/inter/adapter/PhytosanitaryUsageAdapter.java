package ekylibre.zero.inter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ekylibre.zero.R;
import ekylibre.zero.home.Zero;
import ekylibre.zero.inter.model.PhytosanitaryUsage;

public class PhytosanitaryUsageAdapter extends RecyclerView.Adapter<PhytosanitaryUsageAdapter.ViewHolder> {

    private final static String TAG = "UsagesAdapter";
    private final List<PhytosanitaryUsage> usages;


    public PhytosanitaryUsageAdapter(List<PhytosanitaryUsage> usages) {
        this.usages = usages;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.usage_target) TextView target;
        @BindView(R.id.max_dose_data) TextView maxDose;
        @BindView(R.id.max_usage_data) TextView maxUsage;
        @BindView(R.id.dar_data) TextView dar;
        @BindView(R.id.znt_aqua_data) TextView zntAqua;
        @BindView(R.id.znt_arthro_data) TextView zntArthro;
        @BindView(R.id.znt_plant_data) TextView zntPlant;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void display(int pos) {

            PhytosanitaryUsage usage = usages.get(pos);
            Context ctx = itemView.getContext();

            // Set background odd and even
            @ColorRes int colorId = pos %2 == 1 ? R.color.another_light_grey : R.color.white;
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), colorId));

            // Set text
            String status = usage.state.equals("Retrait") ? "[RETIRE] " : "";
            target.setText(String.format("%s%s", status, usage.ephyUsagePhrase));
            maxDose.setText(usage.doseQuantity != null ? String.format(Zero.getLocale(),"%.1f %s", usage.doseQuantity, usage.doseUnitName) : "- -");
            maxUsage.setText(usage.applicationsCount != null ? String.valueOf(usage.applicationsCount) : "- -");
            dar.setText(ctx.getResources().getString(R.string.x_days, usage.harvestDelayDays));
            zntAqua.setText(usage.aquaticBuffer != null ? ctx.getResources().getString(R.string.x_meters,  usage.aquaticBuffer) : "- -");
            zntArthro.setText(usage.arthropodBuffer != null ? ctx.getResources().getString(R.string.x_meters, usage.arthropodBuffer) : "- -");
            zntPlant.setText(usage.plantBuffer != null ? ctx.getResources().getString(R.string.x_meters, usage.plantBuffer) : "- -");
        }
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_phyto_usage, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.display(position);
    }

    @Override
    public int getItemCount() {
        return usages.size();
    }
}
