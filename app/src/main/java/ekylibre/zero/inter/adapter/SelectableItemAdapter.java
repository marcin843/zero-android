package ekylibre.zero.inter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ekylibre.util.pojo.SpinnerItem;
import ekylibre.zero.R;
import ekylibre.zero.inter.fragment.PhytosanitaryDialogFragment;
import ekylibre.zero.inter.model.GenericItem;

import static ekylibre.zero.inter.InterActivity.fragmentManager;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.selectedParamsList;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.selectedVariantsList;


public class SelectableItemAdapter extends RecyclerView.Adapter<SelectableItemAdapter.ViewHolder> {

    private final List<GenericItem> dataset;
    private final String paramType;
    private final String role;
    private boolean isVariant;

    // CONTRUCTOR
    public SelectableItemAdapter(List<GenericItem> dataset, String paramType, String role) {
        this.dataset = dataset;
        this.paramType = paramType;
        this.role = role;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_first_line) TextView firstLine;
        @BindView(R.id.item_second_line) TextView secondLine;
        @BindView(R.id.item_third_line) TextView thirdLine;
        @BindView(R.id.item_info) ImageView infoButton;
        GenericItem item;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            isVariant = role.equals("outputs");

            itemView.setOnClickListener(v -> {

                List<GenericItem> selectedItems = isVariant ? selectedVariantsList : selectedParamsList;

                // Check if current item exists in selectedParamsList
                if (selectedItems.contains(item)) {
                    GenericItem existingItem = selectedItems.get(selectedItems.indexOf(item));
                    // Check if item is selected for curent paramType
                    if (existingItem.referenceName.containsKey(paramType)) {
                        // Item was already selected for this role, so unselect
                        v.setSelected(false);
                        existingItem.referenceName.remove(paramType);
                        if (existingItem.referenceName.isEmpty())
                            selectedItems.remove(existingItem);
                    } else {
                        v.setSelected(true);
                        selectedItems.add(item);
                        existingItem = selectedItems.get(selectedItems.indexOf(item));
                        existingItem.referenceName.put(paramType, role);
                    }
                } else {
                    v.setSelected(true);
                    if (paramType.equals("replacement_part") || paramType.equals("consumable_part")) {
                        item.quantity = new BigDecimal(1);
                        item.unit_spinner = new SpinnerItem("population", null, null);
                    }
                    selectedItems.add(item);
                    GenericItem existingItem = selectedItems.get(selectedItems.indexOf(item));
                    existingItem.referenceName.put(paramType, role);
                }


//                if (item.referenceName.containsKey(paramType)) {
////                    item.isSelected = false;
//                    v.setSelected(false);
//                    item.referenceName.remove(paramType);
//                } else {
////                    item.isSelected = true;
//                    v.setSelected(true);
//                    item.referenceName.put(paramType, role);
//                    if (paramType.equals("replacement_part") || paramType.equals("consumable_part")) {
//                        item.quantity = new BigDecimal(1);
//                        item.unit = "population";
//                    }
//                    Log.i("SelectableItemAdapter", paramType + " " + role);
//                }
                notifyItemChanged(dataset.indexOf(item));
            });
        }

        void display(int position) {

            Context context = itemView.getContext();
            item = dataset.get(position);

            List<GenericItem> selectedItems = isVariant ? selectedVariantsList : selectedParamsList;

            @ColorRes int colorId = position %2 == 1 ? R.color.another_light_grey : R.color.white;
            @ColorRes int textColor = R.color.primary_text;
            if (selectedItems.contains(item)) {
                GenericItem existingItem = selectedItems.get(selectedItems.indexOf(item));
                if (existingItem.referenceName.containsKey(paramType)) {
                    colorId = R.color.basic_blue;
                    textColor = R.color.white;
                }
            }

            firstLine.setText(item.name);
            firstLine.setTextColor(ContextCompat.getColor(context, textColor));

            StringBuilder sb = new StringBuilder();

            if (item.netSurfaceArea != null)
                sb.append(item.netSurfaceArea);
            else if (item.workNumber != null)
                sb.append(item.workNumber);

            if (item.number != null) {
                if (sb.length() > 0)
                    sb.append(" - ");
                sb.append(item.number);
            }

            if (item.containerName != null) {
                if (sb.length() > 0)
                    sb.append(" - ");
                sb.append(item.containerName);
            }

            secondLine.setText(sb.toString());
            secondLine.setTextColor(ContextCompat.getColor(context, textColor));
            itemView.setBackgroundColor(ContextCompat.getColor(context, colorId));

            if (item.population != null) {
                thirdLine.setText(item.population);
                thirdLine.setVisibility(View.VISIBLE);
            } else
                thirdLine.setVisibility(View.GONE);

            if (paramType.equals("plant_medicine") && item.refId != null ) {
                infoButton.setVisibility(View.VISIBLE);
                infoButton.setOnClickListener(view -> {
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    PhytosanitaryDialogFragment phytoDialog = PhytosanitaryDialogFragment.newInstance(item.refId);
                    phytoDialog.show(ft, null);
                });
            } else {
                infoButton.setVisibility(View.GONE);
            }
        }
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // CREATE VIEW HOLDER AND INFLATING ITS XML LAYOUT
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_generic, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.display(position);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
