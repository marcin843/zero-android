package ekylibre.zero.home;

import android.app.Application;
import android.content.Context;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.os.ConfigurationCompat;

import java.text.SimpleDateFormat;
import java.util.Locale;

import ekylibre.util.DateConstant;
import ekylibre.util.realm.Migrations;
import ekylibre.zero.BuildConfig;
import ekylibre.zero.R;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.sentry.core.Sentry;

/**************************************
 * Created by pierre on 9/19/16.      *
 * ekylibre.zero.home for zero-android    *
 *************************************/
public class Zero extends Application {

    public static final String LARRERE = "larrere";

    private static Application app;
    private static Locale currentLocale;

    public static SimpleDateFormat ISO8601;
    public static SimpleDateFormat HOUR_MIN_LOCAL;

    public static String ACCOUNT_TYPE;

    @Override
    public void onCreate() {
        super.onCreate();

        app = this;
        currentLocale = ConfigurationCompat.getLocales(getResources().getConfiguration()).get(0);
        ISO8601 = new SimpleDateFormat(DateConstant.ISO_8601, currentLocale);
        HOUR_MIN_LOCAL = new SimpleDateFormat("HH:mm", currentLocale);

        ACCOUNT_TYPE = getResources().getString(R.string.accountType);

//        StringBuilder accType = new StringBuilder(getResources().getString(R.string.accountType));
//        if (!BuildConfig.FLAVOR.equals(LARRERE))
//            accType.append(".").append(BuildConfig.FLAVOR);
//        ACCOUNT_TYPE = accType.toString();


        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

//        Sentry.init(new AndroidSentryClientFactory(this));
        Sentry.setTag("flavor", BuildConfig.FLAVOR);

        // Configure Realm database instance for storing and querying ontology
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(1)
                .migration(new Migrations())
                .assetFile("ontology.realm")
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public static Context getContext() {
        return app.getApplicationContext();
    }

    public static String getPkgName() {
        return getContext().getPackageName();
    }

    public static Locale getLocale() {
        return currentLocale;
    }
}
