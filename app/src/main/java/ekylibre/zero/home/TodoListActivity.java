package ekylibre.zero.home;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.ListView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import ekylibre.APICaller.Intervention;
import ekylibre.database.ZeroContract.DetailedInterventionAttributes;
import ekylibre.database.ZeroContract.InterventionParameters;
import ekylibre.database.ZeroContract.Interventions;
import ekylibre.util.AccountTool;
import ekylibre.util.UpdatableClass;
import ekylibre.zero.R;
import ekylibre.zero.intervention.InterventionActivity;

import static ekylibre.util.Helper.toHourMin;
import static ekylibre.zero.home.Zero.HOUR_MIN_LOCAL;
import static ekylibre.zero.home.Zero.ISO8601;

/**************************************
 * Created by pierre on 7/8/16.       *
 * ekylibre.zero for zero-android     *
 *************************************/

public class TodoListActivity extends UpdatableClass {

    private static final int CALENDAR_ID = 0;
    private static final int TITLE = 1;
    private static final int DESCRIPTION = 2;
    private static final int DTSTART = 3;
    private static final int DTEND = 4;
    private static final int ALL_DAY = 5;
    private static final int EVENT_LOCATION = 6;
    private static final String TAG = "TodoListAct";

    final static int NONE_CALENDAR = 0;
    final static int LOCAL_CALENDAR = 1;
    final static int EKYLIBRE_CALENDAR = 2;

    private ListView todoListView;
    private Context context;

    /*
    ** This activity display all the tasks of the current day requesting the actual phone calendar
    ** AND ekylibre calendar
    ** Events are stock in ArrayAdapter between today 00h00 & tomorrow 00h00
    ** CONSTANTS are use to get the part of data you want from adapter
    */

    TodoListActivity(final Context context, final Activity activity, ListView foundListView) {

        this.todoListView = foundListView;
        this.context = context;

        List<TodoItem> todolist = createList();

        if (todolist == null)
            return;

        TodoAdapter adapter = new TodoAdapter(context, todolist);

        todoListView.setAdapter(adapter);
        todoListView.setClickable(true);
        todoListView.setOnItemClickListener(
                (adapterView, view, position, id) -> {
                    if (isSync)
                        return;
                    TodoItem item = (TodoItem) adapterView.getItemAtPosition(position);
                    Log.d(TAG, item.getEvent());
                    if (item.getState().equals(InterventionActivity.STATUS_FINISHED)
                            || item.getSource() == TodoListActivity.LOCAL_CALENDAR)
                        return;
                    Intent intent = new Intent(context, InterventionActivity.class);
                    intent.putExtra(Interventions._ID, item.getIntervention_id());
                    context.startActivity(intent);
                });
    }

    public void setListView(ListView listView) {
        todoListView = listView;
    }

    /*
    ** This method will create the todoList as list with an item element
    ** Item element contain the block information which will be interpreted
    ** by the adapter.
    */
    private List<TodoItem> createList() {
        Cursor cursRequested;

//        cursLocal = getEventsFromLocal();
        cursRequested = getEventsFromEK();

        Calendar currentDate = Calendar.getInstance();
        currentDate.setTimeInMillis(0);
        ArrayList<TodoItem> compiledList = getListCompact(cursRequested);

        int i = -1;
        if (compiledList.size() == 0) {
            i += 2;
            compiledList.add(0, new TodoItem(true, getDateOfDay()));
            compiledList.add(1, new TodoItem(true, context.getResources().getString(R.string.no_event)));
        }

        while (++i < compiledList.size()) {
            if (newDay(compiledList.get(i).getDate().getTimeInMillis(), currentDate)) {
                currentDate.setTimeInMillis(compiledList.get(i).getDate().getTimeInMillis());
                compiledList.add(i, new TodoItem(true, currentDate));
            }
        }

        return (compiledList);
    }

    /*
    ** Create a list,
    ** add events from local / ekylibre,
    ** sort events by date asc
    */
    private ArrayList<TodoItem> getListCompact(Cursor cursRequested) {

        ArrayList<TodoItem> list = new ArrayList<>();

//        addLocalEvents(list, cursLocal);
        addEkylibreEvents(list, cursRequested);

        sortList(list);

        return (list);
    }

    /*
    ** Implementation of sort
    ** That will compare dates from events and sort them
    ** Events wil be ordered by asc
    */
    private void sortList(ArrayList<TodoItem> list) {
        Collections.sort(list, (t1, t2) -> Long.compare(t1.getDate().getTimeInMillis(), t2.getDate().getTimeInMillis()));
        int i = -1;
        while (++i < list.size())
            list.get(i).setNumber(i);
    }

    /*
    ** Add events registered in local database which are link with ekylibre
    ** to the list
    */
    private void addEkylibreEvents(ArrayList<TodoItem> list, Cursor cursRequested) {
        while (cursRequested != null && cursRequested.moveToNext()) {
            try {
                Date startDate = ISO8601.parse(cursRequested.getString(DTSTART));
                String startDateFormatted = HOUR_MIN_LOCAL.format(Objects.requireNonNull(startDate));

                String endDateFormatted = null;
                if (!cursRequested.isNull(DTEND))
                    endDateFormatted = toHourMin(cursRequested.getString(DTEND));

                Calendar cal = Calendar.getInstance();
                cal.setTime(startDate);
                String desc = getDescription(cursRequested);
                list.add(new TodoItem(startDateFormatted, endDateFormatted, cursRequested.getString(TITLE),
                        desc, cal, cursRequested.getInt(0), EKYLIBRE_CALENDAR, cursRequested.getString(5)));
            }
            catch (ParseException e) {
                Log.i(TAG, "Error while parsing date from requested intervention  " + e.getMessage());
            }
        }

        if (cursRequested != null && !cursRequested.isClosed())
            cursRequested.close();
    }

    /*
    ** Add events registered in local phone calendar
    ** to the list
    */
//    private void addLocalEvents(ArrayList<TodoItem> list, Cursor cursLocal) {
//
//        String startDateFormatted;
//        String endDateFormatted;
//
//        while (cursLocal != null && cursLocal.moveToNext()) {
//
//            Date startDate = new Date(cursLocal.getLong(DTSTART));
//            Date endDate = new Date(cursLocal.getLong(DTEND));
//            startDateFormatted = hourMin.format(startDate);
//            if (cursLocal.getInt(ALL_DAY) == 0)
//                endDateFormatted = hourMin.format(endDate);
//            else
//                endDateFormatted = "00h00";
//            Calendar cal = Calendar.getInstance();
//            cal.setTime(startDate);
//            list.add(new TodoItem(startDateFormatted, endDateFormatted, cursLocal.getString(TITLE),
//                    cursLocal.getString(DESCRIPTION), cal, LOCAL_CALENDAR));
//        }
//        if (cursLocal != null && !cursLocal.isClosed())
//            cursLocal.close();
//
//    }

    /*
    ** @return: timeZone
    */
//    public static String getTimeZone()
//    {
//        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault());
//        String   timeZone = new SimpleDateFormat("Z").format(calendar.getTime());
//        return (timeZone.substring(0, 3) + ":"+ timeZone.substring(3, 5));
//    }

    /*
    ** Create a string that indent information below envent's title
    ** into a block event
    */
    private String getDescription(Cursor cursRequested) {
        StringBuilder sb = new StringBuilder();
        int id = cursRequested.getInt(0);

        int detailedId = cursRequested.isNull(6) ? -1 : cursRequested.getInt(6);

        // Regular intervention
        if (detailedId == -1) {
            Cursor curs = getInfoForIntervention(id);

            if (curs == null || curs.getCount() == 0)
                return null;

            while (curs.moveToNext()) {
                sb.append("• ").append(curs.getString(1));
                if (!curs.isLast())
                    sb.append("\n");
            }
            if (!curs.isClosed())
                curs.close();
        } else {
            // Detailed intervention
            ContentResolver cr = context.getContentResolver();

            final String where = "role == 'targets' AND detailed_intervention_id == ?";
            String[] args = new String[]{String.valueOf(detailedId)};

            try (Cursor curs = cr.query(DetailedInterventionAttributes.CONTENT_URI,
                    DetailedInterventionAttributes.PROJECTION_TARGET, where, args, null)) {

                if (curs == null)
                    return null;

                while (curs.moveToNext()) {
                    sb.append("• ").append(curs.getString(1));
                    if (!curs.isLast())
                        sb.append("\n");
                }
            }

        }
        return sb.toString();
    }

    private Cursor getInfoForIntervention(int id) {
        Cursor curs;
        ContentResolver contentResolver = context.getContentResolver();

        curs = contentResolver.query(InterventionParameters.CONTENT_URI,
                InterventionParameters.PROJECTION_TARGET,
                InterventionParameters.ROLE + " LIKE " + "\"" + Intervention.TARGET + "\" AND "
                + InterventionParameters.FK_INTERVENTION + " == " + id,
                null, null);

        return (curs);
    }

    private Cursor getEventsFromEK() {
        Cursor curs;
        ContentResolver contentResolver = context.getContentResolver();

        curs = contentResolver.query(Interventions.CONTENT_URI,
                Interventions.PROJECTION_BASIC,
                "user == \"" + AccountTool.getCurrentAccount(context).name + "\" AND (state != 'SYNCED' OR state IS NULL)",
                null, "datetime(started_at)");

        // ZeroContract.Interventions.TYPE + " LIKE " + "\"" + Intervention.REQUESTED + "\" AND "

        return (curs);
    }

    /*
    ** check if date is a new day compared to current reference which is currentDate
    ** if it's return true you should update currentDate then it will be able to
    ** tell you if this is a new day compared to previous one
    */
    private boolean newDay(long date, Calendar currentDate) {
        return currentDate.getTimeInMillis() == 0
                || getDayFromMillis(date) != currentDate.get(Calendar.DAY_OF_YEAR);
    }

    /*
    ** Return new instance of Calendar containing current date
    */
    private Calendar getDateOfDay() {
        Calendar calendar = Calendar.getInstance();
        int year;
        int month;
        int day;
        int hour;
        int minute;
        int second;

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = 0;
        minute = 0;
        second = 0;

        calendar.set(year, month, day, hour, minute, second);
        //Log.d(TAG, "TODAY DATE = " + calendar.getTime());
        return (calendar);
    }

    /*
    ** Return new instance of Calendar containing tomorrow date
    */
//    public Calendar getDateOfTomorrow() {
//        Calendar calendar = Calendar.getInstance();
//        int year;
//        int month;
//        int day;
//        int hour;
//        int minute;
//        int second;
//
//        year = calendar.get(Calendar.YEAR);
//        month = calendar.get(Calendar.MONTH);
//        day = calendar.get(Calendar.DAY_OF_MONTH);
//        hour = 0;
//        minute = 0;
//        second = 0;
//
//        calendar.set(year, month, day, hour, minute, second);
//        calendar.add(Calendar.DAY_OF_YEAR, 1);
//        //Log.d(TAG, "TOMORROW DATE = " + calendar.getTime());
//        return (calendar);
//    }

//    public Calendar getDateOfNextMonth() {
//        Calendar calendar = Calendar.getInstance();
//        int year;
//        int month;
//        int day;
//        int hour;
//        int minute;
//        int second;
//
//        year = calendar.get(Calendar.YEAR);
//        month = calendar.get(Calendar.MONTH);
//        day = calendar.get(Calendar.DAY_OF_MONTH);
//        hour = 0;
//        minute = 0;
//        second = 0;
//
//        calendar.set(year, month, day, hour, minute, second);
//        calendar.add(Calendar.MONTH, 1);
//        if (BuildConfig.DEBUG) Log.d(TAG, "NEXT MONTH DATE = " + calendar.getTime().toString());
//        return (calendar);
//    }

    /*
    ** Set fields we want to request on getEvents from phone calendar
    */
//    private String[] setProjection() {
//        return (new String[]
//                {
//                        CalendarContract.Events.CALENDAR_ID,
//                        CalendarContract.Events.TITLE,
//                        CalendarContract.Events.DESCRIPTION,
//                        CalendarContract.Events.DTSTART,
//                        CalendarContract.Events.DTEND,
//                        CalendarContract.Events.ALL_DAY,
//                        CalendarContract.Events.EVENT_LOCATION
//                });
//    }

    /*
    ** Core of the request send to phone calendar
    */
//    private String setSelection(Calendar startTime, Calendar endTime) {
//        return ("(( " + CalendarContract.Events.DTSTART + " >= " + startTime.getTimeInMillis() +
//                " ) AND ( " + CalendarContract.Events.DTSTART + " <= " + endTime.getTimeInMillis() + " ))");
//    }

    /*
    ** Return Instance of Calendar with a Date passed by argument
    */
//    private Calendar setTime(Date date) {
//        Calendar time;
//
//        time = Calendar.getInstance();
//        time.setTime(date);
//        return (time);
//    }

    /*
    ** Get events from Phone calendar with value set as we want on methods call :
    ** setSelection (Core of the request)
    ** setProjection (Selection of fields to request)
    */
//    @TargetApi(23)
//    public Cursor getEventsFromLocal() {
//        Context context;
//        ContentResolver contentResolver;
//        Calendar startTime;
//        Calendar endTime;
//        String[] projection;
//        String selection;
//        Cursor returnCurs = null;
//
//        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this.context);
//        if (!pref.getBoolean(SettingsActivity.PREF_SYNC_CALENDAR, false))
//            return (null);
//        if (BuildConfig.DEBUG) Log.d(TAG, "Getting events from local calendar");
//        projection = setProjection();
//        startTime = getDateOfDay();
//        endTime = getDateOfNextMonth();
//        startTime.setTimeInMillis(startTime.getTimeInMillis() - 1000);
//        selection = setSelection(startTime, endTime);
//
//        context = this.context;
//        contentResolver = context.getContentResolver();
//        if (!PermissionManager.calendarPermissions(context, this.activity))
//            return (null);
//
//        returnCurs = contentResolver.query(CalendarContract.Events.CONTENT_URI,
//                projection,
//                selection,
//                null,
//                CalendarContract.Events.DTSTART);
//        return (returnCurs);
//    }

    /*
    ** Pure log of local events
    ** Use this to debug
    */
//    public void         affEvents(Cursor curs)
//    {
//        int             it;
//
//        if (!BuildConfig.DEBUG)
//            return;
//        it = 0;
//        if (curs == null)
//            curs = getEventsFromLocal();
//        if (curs != null && curs.moveToFirst())
//        {
//            do
//            {
//                ++it;
//                Log.d(TAG, "==========");
//                Log.d(TAG, "ENVENT NUM " + it);
//                Log.d(TAG, "ID = " + curs.getString(CALENDAR_ID));
//                Log.d(TAG, "DESC = " + curs.getString(DESCRIPTION));
//                Log.d(TAG, "DATE START = " + curs.getString(DTSTART));
//                Log.d(TAG, "DATE END = " + curs.getString(DTEND));
//                Log.d(TAG, "ALL DAY = " + curs.getString(ALL_DAY));
//                Log.d(TAG, "EVT LOCATION = " + curs.getString(EVENT_LOCATION));
//                Log.d(TAG, "==========");
//            } while (curs.moveToNext());
//        }
//    }

    private int getDayFromMillis(long time) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);
        return (cal.get(Calendar.DAY_OF_YEAR));
    }
}

