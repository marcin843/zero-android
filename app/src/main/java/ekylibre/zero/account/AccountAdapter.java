package ekylibre.zero.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import ekylibre.service.LongSyncService;
import ekylibre.zero.R;
import ekylibre.util.AccountTool;
import ekylibre.zero.home.MainActivity;

import static ekylibre.service.LongSyncService.ACTION_SYNC_ALL;
import static ekylibre.zero.account.AccountManagerActivity.CURRENT_ACCOUNT_NAME;

/**************************************
 * Created by pierre on 7/18/16.      *
 * ekylibre.zero for zero-android     *
 *************************************/
public class AccountAdapter extends ArrayAdapter<Account> {

    private Context context;
    private AccountManagerActivity accountManagerActivity;

    AccountAdapter(Context context, ArrayList<Account> accountList, AccountManagerActivity accountManagerActivity) {
        super(context, 0, accountList);
        this.context = context;
        this.accountManagerActivity = accountManagerActivity;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull final ViewGroup parent) {

        if (convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.account_item, parent, false);

        AccountViewHolder viewHolder = (AccountViewHolder) convertView.getTag();
        if (viewHolder == null) {
            viewHolder = new AccountViewHolder();
            viewHolder.accountName = convertView.findViewById(R.id.accountName);
            viewHolder.accountInstance = convertView.findViewById(R.id.accountInstance);
            viewHolder.delete = convertView.findViewById(R.id.deleteAccount);
            viewHolder.avatar = convertView.findViewById(R.id.avatar_account);
            viewHolder.layout = convertView.findViewById(R.id.account_item);
            convertView.setTag(viewHolder);
        }

        final Account item = getItem(position);
        viewHolder.accountName.setText(AccountTool.getAccountName(item, context));
        viewHolder.accountInstance.setText(AccountTool.getAccountInstance(item, context));
        viewHolder.delete.setVisibility(View.VISIBLE);
        viewHolder.avatar.setVisibility(View.VISIBLE);

        viewHolder.delete.setOnClickListener(view -> {
            Account selectedAccount = getItem(position);
            if (selectedAccount != null && selectedAccount.equals(AccountTool.getCurrentAccount(context)))
                AccountTool.setFirstAccountPref(PreferenceManager.getDefaultSharedPreferences(context), context);
            AccountManager manager = AccountManager.get(context);
            remove(selectedAccount);
            manager.removeAccount(selectedAccount, null, null);
            notifyDataSetChanged();
        });

        viewHolder.layout.setOnClickListener(view -> {
            Account newCurrAccount;
            newCurrAccount = item;
            if (newCurrAccount == null)
                return;

            SharedPreferences preferences = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_MULTI_PROCESS);
            preferences.edit().putString(CURRENT_ACCOUNT_NAME, newCurrAccount.name).apply();

            Log.d("Account manager", "New current account is ==> " + newCurrAccount.name);
//            accountManagerActivity.syncAll(newCurrAccount);

            Intent intent = new Intent(context, LongSyncService.class);
            intent.putExtra("receiver", MainActivity.receiver);
            intent.setAction(ACTION_SYNC_ALL);
            context.startService(intent);

            accountManagerActivity.finish();
        });
        return (convertView);
    }}
