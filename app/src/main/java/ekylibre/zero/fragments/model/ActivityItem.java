package ekylibre.zero.fragments.model;

public class ActivityItem {
    public int id;
    public String name;
    public String variety;
    public String details;

    public ActivityItem(int id, String name, String variety,String details) {
        this.id = id;
        this.name = name;
        this.variety = variety;
        this.details = details;
    }
}