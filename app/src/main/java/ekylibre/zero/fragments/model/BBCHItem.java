package ekylibre.zero.fragments.model;

public class BBCHItem {
    public int id;
    public String name;

    public BBCHItem(int id, String name) {
        this.id = id;
        this.name = name;
    }
}